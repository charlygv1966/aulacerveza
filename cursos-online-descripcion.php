<?php
session_start();
header('Content-Type: text/html; charset=UTF-8');
include("inc/funciones.php");
$id_curso = $_GET["curso"];
$pagina = $_GET["pagina"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Cursos online</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="cursos">
			<div id="listado">
				<h2><img src="images/cursos/online.png" alt="ONLINE"></h2>
				<section id="curso">
					<?php
					$query="SELECT * FROM cursos WHERE id=".$id_curso." AND publicado='si'";
					$result=mysql_query($query);
					while ($row=mysql_fetch_array($result)) {
					?>
					<article>
					<figure>
							<img src="images/cursos/online/<?php echo $row["imagen"]; ?>" alt="<?php echo utf8_encode($row["titulo"]); ?>" />
							<figcaption><?php echo utf8_encode($row["titulo"]); ?></figcaption>
						</figure>
						<div class="textos">
							<h2><?php echo utf8_encode($row["titulo"]); ?></h2>
							<h3><?php echo fecha_foro($row["fecha_comienzo"]); ?> - <?php echo fecha_foro($row["fecha_final"]); ?></h3>
							<?php echo $row["lugar_realizacion"]; ?>
							<p><strong><?php echo "Precio: ".number_format($row["precio_curso"],2,',','.')." &euro;"; ?></strong></p>
							<?php echo $row["descripcion"]; ?>
						</div>

						<div class="registrocurso">
						<?php
						if ($_SESSION["entrar"] == "SI") {
						?>
						<div class="inscripcion">
							<p><a href="cursos-online-reservas.php?curso=<?php echo $row["id"]; ?>" title="INSCRIPCIÓN">INSCRIPCIÓN</a></p>
						</div>
						<?php } else { ?>
							<div class="registrarcurso">
								<p>Debes estar <a href="registro.php" title="Ir a la página de registro">registrado</a> para poder inscribirte en el curso. Si ya tienes cuenta de usuario valídate y continua con el proceso de inscripción.</p>
							</div>
						<?php } ?>
						</div>
					</article>
					<?php
					}
					?>
				</section>
			</div>

			<div class="navegacion">
				<ul>
					<li class="anterior"><a href="cursos-online.php?pagina=<?php echo $pagina; ?>" title="volver">volver</a></li>
				</ul>
			</div>

		</section>
	</section>
</div>

</body>
</html>

