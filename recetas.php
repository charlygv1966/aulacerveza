<?php
header('Content-Type: text/html; charset=UTF-8');
include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Recetas</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="recetas">
			<article>
				<?php
				//primero veo cuantos registros hay que mostrar
				$query_listado = "SELECT recetas FROM configuracion_general";
				$result_listado=mysql_query($query_listado);
				while ($row_listado=mysql_fetch_array($result_listado)) {
					$total_listado_recetas = $row_listado["recetas"];
				}
				//consulta para la paginación
				$query0 = "SELECT * FROM recetas WHERE publicado='si' ORDER BY fecha_actualizacion";
				$result0=mysql_query($query0);
				$num_total_registros = mysql_num_rows($result0);
				//defino cantidad de artículos por página
				$TAMANO_PAGINA = $total_listado_recetas;
				//calculo el total de páginas
				$total_paginas = ceil($num_total_registros / $TAMANO_PAGINA);
				//examino la página a mostrar y el inicio del registro a mostrar
				$pagina = $_GET["pagina"];
				if (!$pagina || $pagina > $total_paginas) {
					$inicio = 0;
					$pagina=1;
				} else {
					$inicio = ($pagina - 1) * $TAMANO_PAGINA;
				}

				$query="SELECT * FROM recetas_imagen ORDER BY fecha_actualizacion";
				$result=mysql_query($query);
				while ($row=mysql_fetch_array($result)) {
				?>
				<figure>
					<img src="images/recetas/<?php echo $row["imagen"]; ?>" alt="Recetas" />
					<figcaption>Recetas</figcaption>
				</figure>
				<?php
				}
				?>
				<div class="textos">
					<h2><img src="images/recetas/recetas.png" alt="Recetas"></h2>
					<ul>
					<?php
					$query2="SELECT id,titulo FROM recetas WHERE publicado='si' ORDER BY fecha_actualizacion LIMIT " . $inicio . "," . $TAMANO_PAGINA;
					$result2=mysql_query($query2);
					if ($num_total_registros > 0) {
						while ($row2=mysql_fetch_array($result2)) {
						?>
							<li><a href="recetas-descripcion.php?receta=<?php echo $row2["id"]; ?>&pagina=<?php echo $pagina; ?>"><?php echo utf8_encode($row2["titulo"]); ?></a></li>
						<?php
						}
					} else {
					echo "<p>NO SE HAN ENCONTRADO REGISTROS</p>";
					}
					mysql_close($link);
					?>
					</ul>
				</div>
			</article>

			<?php
			if ($total_paginas > 1){
			?>
			<div class="navegacion">
				<ul>
					<?php
					// PÁGINA ANTERIOR
					if ($pagina > 1) {
						echo "<li class=\"anterior\"><a href='".$_SERVER['PHP_SELF']."?pagina=" . ($pagina - 1) ."' title=\"Página anterior\">anterior</a></li>";
					} else {
						echo "<li class=\"desactivado\">anterior</li>";
					}

					// PÁGINA SIGUIENTE
					if ($pagina < $total_paginas) {
						echo "<li class=\"siguiente\"><a title=\"Página siguiente\" href='".$_SERVER['PHP_SELF']."?pagina=" . ($pagina + 1) . "'>siguiente</a></li>";
					} else {
						echo "<li class=\"desactivado\">siguiente</li>";
					}
					?>
				</ul>
			</div>
			<?php
			}
			?>

		</section>
	</section>
</div>

</body>
</html>

