<?php
header('Content-Type: text/html; charset=UTF-8');
include("inc/funciones.php");
$recurso = $_GET["recurso"];
$pagina = $_GET["pagina"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Recursos</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="recursos">
			<section id="recurso">
				<article>
					<?php
					$query="SELECT * FROM recursos WHERE id='".$recurso."' ORDER BY fecha_actualizacion";
					$result=mysql_query($query);
					while ($row=mysql_fetch_array($result)) {
					?>
					<figure>
						<img src="images/recursos/secciones/<?php if ($row["imagen"] != "") { echo $row["imagen"]; } else { echo "resource.jpg"; } ?>" alt="" />
						<figcaption>Recurso</figcaption>
					</figure>
					<div class="textos">
						<h2><?php echo utf8_encode($row["titulo"]); ?></h2>
						<?php echo utf8_encode($row["textos"]); ?>
					</div>
					<?php
					}
					mysql_close($link);
					?>
					<div class="navegacion">
						<ul>
							<li><a href="recursos.php?pagina=<?php echo $pagina; ?>" title="volver">volver</a></li>
						</ul>
					</div>
				</article>
			</section>
		</section>
	</section>
</div>

</body>
</html>

