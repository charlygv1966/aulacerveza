<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
	$blog = $_GET["blog"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>BLOG (Modificar contenidos)</h1>
		<h2><?php echo fecha(); ?></h2>
		<form method="post" accept-charset="utf-8" action="blog-textos-actualizar.php">
			<?php
			$query="SELECT * FROM blog WHERE id=".$blog." ORDER BY fecha_publicacion";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
				<input type="hidden" name="id_blog" id="id_blog" value="<?php echo $row["id"]; ?>" />
				<input type="hidden" name="publicado" id="publicado" value="<?php echo $row["publicado"]; ?>" />
				<p>
					<label for="titulo">Título del blog</label>
					<input type="text" name="titulo" id="titulo" placeholder="Introduce el título del blog" value="<?php if ($row) { echo utf8_encode($row["titulo"]); } else { echo utf8_encode($titulo); } ?>" />
				</p>
				<p>
					<label for="textos">Texto del artículo</label>
					<textarea id="blogtextos" name="blogtextos"><?php echo $row["textos"]; ?></textarea>
				</p>
				<br />
				<p>
					<label for="publicado">Publicado</label>
					<select name="publicado" id="publicado">
						<option value="si" <?php if ($row["publicado"] == "si") { echo "selected=\"selected\""; } ?>>Si</option>
						<option value="no" <?php if ($row["publicado"] == "no") { echo "selected=\"selected\""; } ?>>No</option>
				</select>
				</p>
				<p>
					<button type="reset" id="cancelarblog" name="cancelarblog">VOLVER</button>
					<button type="submit" id="blogs" name="blogs">MODIFICAR</button>
				</p>
				<div class="separator"></div>
				<p>
					<a class="boton" href="blog-imagen-actualizar.php?id_blog=<?php echo $row["id"]; ?>"> AÑADIR o MODIFICAR IMAGEN</a>
				</p>
			<?php
			}
			?>
		</form>

		<div class="mensajes">
			<?php if ($_GET["mensaje"] == "2") {
				echo "<p>Has modificado el contenido de la sección</p>";
			} elseif ($_GET["mensaje"] == "3") {
				echo "<p>Se ha producido un error</p>";
			} else {
				echo "<p>&nbsp;</p>";
			}
			?>
		</div>

	</div>
</div>
</body>

</html>
