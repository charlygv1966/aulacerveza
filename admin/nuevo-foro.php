<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");

	if ($_POST) {
		$forotitulo = $_POST["forotitulo"];
		$foropublicado = $_POST["foropublicado"];
		$msg=$_POST["msg"];
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>FOROS (Nuevo foro)</h1>
		<h2><?php echo fecha(); ?></h2>
		<form method="post" accept-charset="utf-8" action="nuevo-foro-actualizar.php">
			<p>
				<label for="forotitulo">Título</label>
				<input type="text" name="forotitulo" id="forotitulo" placeholder="Introduce el título del foro" value="<?php echo $forotitulo; ?>" />
			</p>
			<p>
				<label for="foropublicado">Publicado</label>
				<select name="foropublicado" id="foropublicado">
					<option value="si" <?php if ($foropublicado == "si") { echo "selected=\"selected\""; } ?>>Si</option>
					<option value="no" <?php if ($foropublicado == "no") { echo "selected=\"selected\""; } ?>>No</option>
				</select>
			</p>
			<p>
				<button type="reset" id="cancelar" name="cancelar">VOLVER</button>
				<button type="submit" id="foros" name="foros">ACEPTAR</button>
			</p>
		</form>

		<div class="mensajes">
		<?php
			$mensajeenviado = $_GET["mensaje"];
			switch ($mensajeenviado) {
				case '2':
					$mensaje = "Has publicado un nuevo foro";
					break;
				case '3':
					$mensaje = "Se ha producido un error";
					break;
				default:
					$mensaje = "";
					break;
			}
			echo $mensaje;
			if ($msg=="" && (!isset($_GET["mensaje"]))) {
				$msg = "Debes incluir correctamente todos los campos";
			}
			echo $msg;
		?>
		</div>

	</div>
</div>
</body>

</html>
