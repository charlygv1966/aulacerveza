<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>HOME</h1>
		<?php
		$titulo_derecha = trim(utf8_decode(normalizacion_textos($_POST["titulo_derecha"])));
		$texto_derecha = trim($_POST["texto_derecha"]);
		$envio=1;

		if (esVacio($titulo_derecha)) {
			$envio=0;
			$msg="Introduce el título del banner de la derecha";
		} else if(esVacio($texto_derecha)){
			$envio=0;
			$msg="Introduce el texto del banner de la derecha";
		}

		if ($envio) {
			$query = "UPDATE home SET titulo_derecha='$titulo_derecha',texto_derecha='$texto_derecha'";
			$result=mysql_query($query);
			mysql_close($link);
				if ($result){
				?>
				<script type="text/javascript">
					document.location.href="home-textos-derecha.php?mensaje=ok";
				</script>
				<?php
				} else {
				?>
				<script type="text/javascript">
					document.location.href="home-textos-derecha.php?mensaje=error";
				</script>
				<?php
				}
		} else {
		?>
		<script type="text/javascript">
			document.location.href="home-textos-derecha.php?mensaje=error";
		</script>
		<?php
		}
		?>
	</div>
</div>
</body>

</html>
