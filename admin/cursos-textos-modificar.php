<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
	$curso = $_GET["curso"];
	$msg = $_POST["msg"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>CURSOS (Modificar contenidos)</h1>
		<h2><?php echo fecha(); ?></h2>
		<form method="post" accept-charset="utf-8" action="cursos-textos-actualizar.php">
			<?php
			$query="SELECT * FROM cursos WHERE id=".$curso;
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
				<input type="hidden" name="id_curso" id="id_curso" value="<?php echo $row["id"]; ?>" />
				<p>
					<label for="titulo">Nombre del curso</label>
					<input type="text" name="cursotitulo" id="cursotitulo" placeholder="Introduce el nombre del curso" value="<?php if ($row) { echo utf8_encode($row["titulo"]); } else { echo utf8_encode($titulo); } ?>" />
				</p>
				<p>
					<label for="textos">Descripción</label>
					<textarea id="cursodescripcion" name="cursodescripcion"><?php echo $row["descripcion"]; ?></textarea>
				</p>
				<br />
				<p>
					<label for="titulo">Lugar de realización (Si es un curso ONLINE puede dejarse vacío)</label>
					<textarea name="cursolugar" id="cursolugar"><?php echo $row["lugar_realizacion"]; ?></textarea>
				</p>
				<br />
				<p>
					<label for="titulo">Número de plazas</label>
					<input type="text" name="cursoplazas" id="cursoplazas" placeholder="Introduce el número de plazas del curso" value="<?php echo $row["plazas"]; ?>" class="tipocurso" />
				</p>
				<p>
					<label for="titulo">Forma de pago</label>
					<input type="text" name="cursoforma_pago" id="cursoforma_pago" value="PAGO ÚNICO" class="tipocurso" readonly="readonly" />
				</p>
				<p>
					<label for="titulo">Tipo del curso</label>
					<select name="cursotipo" id="cursotipo" class="tipocurso">
						<option value="PRESENCIAL" <?php if ($row["tipo_curso"] == "PRESENCIAL") { echo "selected=\"selected\""; } ?>>PRESENCIAL</option>
						<option value="ONLINE" <?php if ($row["tipo_curso"] == "ONLINE") { echo "selected=\"selected\""; } ?>>ONLINE</option>
					</select>
				</p>
				<p>
					<label for="titulo">Precio del curso</label>
					<input type="text" name="cursoprecio" id="cursoprecio" placeholder="Introduce el precio del curso" value="<?php echo visualizacion_precios($row["precio_curso"]); ?>" class="tipocurso" /> &euro;
				</p>
				<p>
					<label for="titulo">Fecha de comienzo del curso</label>
					<input type="text" name="cursocomienzo" id="cursocomienzo" placeholder="Introduce la fecha de inicio del curso" value="<?php echo fecha_curso($row["fecha_comienzo"]); ?>" class="tipocurso" /> <span>(fecha actualmente grabada)</span>
				</p>
				<p>
					<label for="titulo">Fecha de finalización del curso</label>
					<input type="text" name="cursofinal" id="cursofinal" placeholder="Introduce la fecha de finalización del curso" value="<?php echo fecha_curso($row["fecha_final"]); ?>" class="tipocurso" /> <span>(fecha actualmente grabada)</span>
				</p>
				<p>
					<label for="recursopublicado">Publicado</label>
					<select name="cursopublicado" id="cursopublicado">
						<option value="si" <?php if ($row["publicado"] == "si") { echo "selected=\"selected\""; } ?>>Si</option>
						<option value="no" <?php if ($row["publicado"] == "no") { echo "selected=\"selected\""; } ?>>No</option>
					</select>
				</p>
				<p>
					<button type="reset" id="cancelarcursos" name="cancelarcursos">VOLVER</button>
					<button type="submit" id="cursos" name="cursos">MODIFICAR</button>
				</p>
				<div class="separator"></div>
				<p>
					<a class="boton" href="curso-imagen-actualizar.php?id_curso=<?php echo $row["id"]; ?>"> AÑADIR o MODIFICAR IMAGEN</a>
				</p>
			<?php
			}
			?>
		</form>

		<div id="mensajes" class="mensajes">
		<?php
			$mensajeenviado = $_GET["mensaje"];
			switch ($mensajeenviado) {
				case '2':
					$mensaje = "Has actualizado el contenido";
					break;
				case '3':
					$mensaje = "Se ha producido un error";
					break;
				default:
					$mensaje = "";
					break;
			}
			echo $mensaje;
			if ($msg=="" && (!isset($_GET["mensaje"]))) {
				$msg = "Debes incluir correctamente todos los campos";
			}
			echo $msg;
		?>
		</div>

	</div>
</div>
</body>

</html>
