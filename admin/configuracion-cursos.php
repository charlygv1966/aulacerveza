<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");

	if ($_POST) {
		$configuracion_cursos = $_POST["configuracion_cursos"];
		$msg=$_POST["msg"];
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>CONFIGURACIÓN GENERAL DE OPCIONES (Número de cursos por página)</h1>
		<h2><?php echo fecha(); ?></h2>
		<h3>ESTA PÁGINA DEL GESTOR PERMITE CONFIGURAR EL NÚMERO DE CURSOS QUE SE MUESTRAN POR PÁGINA, PERMITIENDO NAVEGAR ENTRE LOS MISMOS MEDIANTE LOS ENLACES DE <strong>ANTERIOR</strong> Y <strong>SIGUIENTE</strong></h3>
		<h3><strong>NOTA:</strong> ESTA CONFIGURACIÓN ES PARA LA PÁGINA DE CURSOS PÚBLICA NO PARA EL GESTOR DE CONTENIDOS</h3>
		<form method="post" action="configuracion-cursos-modificar.php" accept-charset="utf-8">
			<?php
			$query="SELECT cursos FROM configuracion_general";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
				<p>
					<label for="configuracion_cursos">Número de cursos por página</label>
					<input type="text" name="configuracion_cursos" id="configuracion_cursos" placeholder="Introduce la cantidad de registros por página" value="<?php if ($row) { echo $row["cursos"]; } else { echo $configuracion_cursos; } ?>" />
				</p>
				<button type="reset" id="cancelar" name="cancelar">VOLVER</button>
				<button type="submit" id="contacto" name="contacto">MODIFICAR</button>
			<?php
			}
			?>
		</form>
		<div class="mensajes">
		<?php
			$mensajeenviado = $_GET["mensaje"];
			switch ($mensajeenviado) {
				case '2':
					$mensaje = "Has actualizado el contenido";
					break;
				case '3':
					$mensaje = "Se ha producido un error";
					break;
				default:
					$mensaje = "";
					break;
			}
			echo $mensaje;
			if ($msg=="" && (!isset($_GET["mensaje"]))) {
				$msg = "Debes incluir correctamente todos los campos";
			}
			echo $msg;
		?>
		</div>
	</div>
</div>
</body>

</html>
