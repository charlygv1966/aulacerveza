<?php
session_start();
header('Content-Type: text/html; charset=UTF-8');
include("inc/funciones.php");
if (!$_SESSION["entrar"] == "SI") {
	header("Location:cursos-presenciales.php");
}
$id_curso = $_GET["curso"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Cursos presenciales</title>
<link rel="stylesheet" href="css/jquery-ui-1.11.1.css" />
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-ui-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="cursos">
			<div id="listado">
				<h2><img src="images/cursos/presenciales.png" alt="PRESENCIALES"></h2>
				<section id="curso">
					<?php
					$query="SELECT * FROM cursos WHERE id=".$id_curso." AND publicado='si'";
					$result=mysql_query($query);
					while ($row=mysql_fetch_array($result)) {
					?>
					<article>
						<figure>
							<img src="images/cursos/presencial/<?php echo $row["imagen"]; ?>" alt="<?php echo utf8_encode($row["titulo"]); ?>" />
							<figcaption><?php echo utf8_encode($row["titulo"]); ?></figcaption>
						</figure>
						<div class="textos reservas">
							<h2>Reserva del curso:</h2>
							<div class="caracteristicas-curso">
								<h3><?php echo utf8_encode($row["titulo"]); ?></h3>
								<div class="mensajes">
								<?php
									$mensajeenviado = $_GET["mensaje"];
									switch ($mensajeenviado) {
										case '1':
											$mensaje = "<p><strong>Error</strong></p><p>Ya hay una plaza reservada en este curso con el mismo nombre.<br /><br />Si quieres apuntar a otra persona deberás indicar otro nombre.</p>";
											break;
										case '2':
											$mensaje = "<p>Has reservado una plaza en el curso.</p>";
											break;
										case '3':
											$mensaje = "<p>Se ha producido un error.</p>";
											break;
										default:
											$mensaje = "";
											break;
									}
									echo $mensaje;
								?>
								</div>
							</div>
						</div>
					</article>
					<?php
					}
					?>
				</section>
			</div>

			<div class="navegacion">
				<ul>
					<li class="anterior"><a href="cursos-presenciales.php" title="volver">volver</a></li>
				</ul>
			</div>

		</section>
	</section>
</div>

</body>
</html>

