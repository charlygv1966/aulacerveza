<?php
header('Content-Type: text/html; charset=UTF-8');
include("admin/inc/conexion.php");
include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Cursos online</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
	<?php
	$id_curso = $_POST["id_curso"];
	$id_usuario = $_POST["id_usuario"];
	$persona_reserva = trim($_POST["persona_reserva"]);
	$envio=1;

	if (esVacio($persona_reserva)) {
		$envio=0;
		$msg="Debes indicar un nombre para poder realizar la reserva del curso";
	}

	//si envio OK
	if ($envio) {
		$query="SELECT COUNT(*) AS cuantos FROM cursos_reservas WHERE id_curso=".$id_curso." AND id_usuario=".$id_usuario;
		$result=mysql_query($query);
		$row=mysql_fetch_array($result);
		$cuantos=$row["cuantos"];

		if ($cuantos){ ?>
			<script type="text/javascript">
				document.location.href="cursos-online-reservas4.php?curso=<?php echo $id_curso; ?>&mensaje=1";
			</script>
		<?php } else {
			$query="INSERT INTO cursos_reservas (id_curso, id_usuario, persona_reserva, fecha_reserva) VALUES (".$id_curso.",".$id_usuario.",'".utf8_decode(normalizacion($persona_reserva))."',now())";
			$result2=mysql_query($query);
			if ($result2){ ?>
				<script type="text/javascript">
					document.location.href="cursos-online-reservas4.php?curso=<?php echo $id_curso; ?>&mensaje=2";
				</script>
			<?php } else { ?>
				<script type="text/javascript">
					document.location.href="cursos-online-reservas4.php?curso=<?php echo $id_curso; ?>&mensaje=3";
				</script>
			<?php }
		}
	} else {
	?>
		<script type="text/javascript">
			document.location.href="cursos-online-reservas4.php?curso=<?php echo $id_curso; ?>";
		</script>
	<?php
	}
	?>

</body>
</html>