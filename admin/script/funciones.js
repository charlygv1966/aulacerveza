$(document).ready(function() {
    $('#menudeopciones').accordion({
        animated: 'bounceslide',
        active: false,
        autoHeight: true,
        collapsible: true,
        navigation: true
    });

    //configuración general de opciones para campos numéricos
    $('input[name=configuracion_cursos]').bind('keypress', function(event) {
        if (event.which < 48 || event.which > 57) {
            return false;
        }
    });

    $('input[name=cursoplazas]').bind('keypress', function(event) {
        if (event.which < 48 || event.which > 57) {
            return false;
        }
    });

    $('input[name=cursoprecio]').bind('keypress', function(event) {
        //se permite el . (punto) para los decimales
        //if (event.which < 45 || event.which > 57) {
        if (event.which < 44 || event.which > 57) {
            return false;
        }
    });

    $('input[name=configuracion_recursos]').bind('keypress', function(event) {
        if (event.which < 48 || event.which > 57) {
            return false;
        }
    });

    $('input[name=configuracion_blog]').bind('keypress', function(event) {
        if (event.which < 48 || event.which > 57) {
            return false;
        }
    });
    //fin configuración general de opciones para campos numéricos

    //home
    $('#texto_izquierda').ckeditor();
    $('#texto_central').ckeditor();
    $('#texto_derecha').ckeditor();
    $('#homeimagen').bind('click', function() {
        comprueba_extension($('[name="imagen"]').val());
    });

    //quienes somos
    $('#quienes-somos').ckeditor();
    $('#quienesomosimagen').bind('click', function() {
        comprueba_extension($('[name="imagen"]').val());
    });

    //videos
    $('#videostextos').ckeditor();
    $('#videosimagen').bind('click', function() {
        comprueba_extension($('[name="imagen"]').val());
    });

    //cursos
    $('#cursodescripcion').ckeditor();
    $('#cursolugar').ckeditor({ resize_enabled: false, height: '100px', toolbar: [
            ['Bold', 'Italic']
        ] });

    $('#cursocomienzo, #cursofinal').datepicker({
        dateFormat: 'dd-mm-yy',
        gotoCurrent: 'true',
        dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        nextText: 'Mes siguiente',
        prevText: 'Mes anterior',
        showOtherMonths: 'true',
        firstDay: '1'
    });
    $('#cursoimagen').bind('click', function() {
        comprueba_extension($('[name="imagen"]').val());
    });
    $('button#cancelarcursos').bind('click', function() {
        document.location.href = "cursos-textos.php";
    });

    //recursos
    $('#recursotextos').ckeditor();
    $('#recursosimagen').bind('click', function() {
        comprueba_extension($('[name="imagen"]').val());
    });
    $('#recursoimagen').bind('click', function() {
        comprueba_extension($('[name="imagen"]').val());
    });
    $('button#cancelarrecursos').bind('click', function() {
        document.location.href = "recursos-textos.php";
    });

    //recetas
    $('#recetatextos').ckeditor();
    $('#recetasimagen').bind('click', function() {
        comprueba_extension($('[name="imagen"]').val());
    });
    $('#recetaimagen').bind('click', function() {
        comprueba_extension($('[name="imagen"]').val());
    });
    $('button#cancelarrecetas').bind('click', function() {
        document.location.href = "recetas-textos.php";
    });

    //secciones
    $('button#cancelarsecciones').bind('click', function() {
        document.location.href = "secciones.php";
    });

    //blogs
    $('#blogtextos').ckeditor();
    $('#blogimagen').bind('click', function() {
        comprueba_extension($('[name="imagen"]').val());
    });
    $('button#cancelarblog').bind('click', function() {
        document.location.href = "blog-textos.php";
    });

    //foros
    $('#forotextos').ckeditor();
    $('#forosimagen').bind('click', function() {
        comprueba_extension($('[name="imagen"]').val());
    });
    $('button#cancelarforos').bind('click', function() {
        document.location.href = "foros-textos.php";
    });

    //fórmulas
    $('#formulastextos').ckeditor();
    $('#formulasimagen').bind('click', function() {
        comprueba_extension($('[name="imagen"]').val());
    });

    //este boton vale para cancelar en todas las secciones
    $('button#volver').bind('click', function() {
        document.location.href = "cursos-reservas.php";
    });

    $('button#volverforos').bind('click', function() {
        document.location.href = "foros-textos-comentarios.php";
    });

    $('button#cancelar').bind('click', function() {
        document.location.href = "home.php";
    });

    //comprueba el formato de las fotos para las diferentes secciones
    function comprueba_extension(archivo) {
        extensiones_permitidas = new Array(".jpg", ".jpeg");
        mierror = "";
        if (!archivo) {
            //Si no tengo archivo, es que no se ha seleccionado un archivo en el formulario
            mierror = "No has seleccionado ningún archivo";
        } else {
            //recupero la extensión de este nombre de archivo
            extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
            //alert (extension);
            //compruebo si la extensión está entre las permitidas
            permitida = false;
            for (var i = 0; i < extensiones_permitidas.length; i++) {
                if (extensiones_permitidas[i] == extension) {
                    permitida = true;
                    break;
                }
            }
            if (!permitida) {
                mierror = "Sólo se pueden subir archivos con extensiones: " + extensiones_permitidas.join();
                $('[name="imagen"]').val('');
            } else {
                //submit!
                //alert ("Todo correcto. Voy a enviar el formulario.");
                document.forms[0].submit();
                return 1;
            }
        }
        //si estoy aqui es que no se ha podido enviar
        alert(mierror);
        return 0;
    }

});