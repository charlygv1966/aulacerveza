<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>CONTACTO (Textos)</h1>
		<?php
		$emailcontacto = trim($_POST["emailcontacto"]);
		$telefonocontacto = trim($_POST["telefonocontacto"]);
		$textocontacto = trim($_POST["textocontacto"]);
		$envio=1;

		if (esVacio($emailcontacto)) {
			$envio=0;
			$msg="Introduce el email de contacto";
		} else if(!esVacio($emailcontacto) && !esEmail($emailcontacto)){
			$envio=0;
			$msg="Introduce un email válido";
		} else if(esVacio($telefonocontacto)){
			$envio=0;
			$msg="Introduce el teléfono de contacto";
		} else if(!esVacio($telefonocontacto) && esTelefono($telefonocontacto)){
			$envio=0;
			$msg="Introduce un teléfono válido";
		} else if(esVacio($textocontacto)){
			$envio=0;
			$msg="Introduce el texto para el pie de contacto";
		}

		if ($envio) {
			$query = "UPDATE contacto SET contacto_email='$emailcontacto',contacto_telefono='$telefonocontacto',contacto_pietexto='$textocontacto',fecha_actualizacion=now()";
			$result=mysql_query($query);
			mysql_close($link);
				if ($result){
				?>
				<script type="text/javascript">
					document.location.href="contacto-textos.php?mensaje=2";
				</script>
				<?php
				} else {
				?>
				<script type="text/javascript">
					document.location.href="contacto-textos.php?mensaje=3";
				</script>
				<?php
				}
		} else {
		?>
		<form method="post" action="contacto-textos.php" id="form" name="form" accept-charset="utf-8">
		<input type="hidden" name="emailcontacto" value="<?php echo $emailcontacto; ?>" />
		<input type="hidden" name="telefonocontacto" value="<?php echo $telefonocontacto; ?>" />
		<input type="hidden" name="textocontacto" value="<?php echo $textocontacto; ?>" />
		<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
		</form>
		<script type="text/javascript">
			document.form.submit();
		</script>
		<?php
		}
		?>
	</div>
</div>
</body>

</html>
