<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>LISTADO DE CONTACTOS (usuarios que se han puesto en contacto por email)</h1>
		<h2><?php echo fecha(); ?></h2>

		<table summary="Listado de contactos" class="contactos">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Email</th>
				<th>Teléfono</th>
				<th>Asunto</th>
				<th>Mensaje</th>
				<th>Contacto</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$query="SELECT * FROM contactos ORDER BY fecha_contacto";
		$result=mysql_query($query);
		$total = mysql_num_rows($result);
		while ($row=mysql_fetch_array($result)) {
		?>
			<tr>
				<td><?php echo utf8_encode($row["id"]); ?></td>
				<td><?php echo utf8_encode($row["nombre"]); ?></td>
				<td><?php echo utf8_encode($row["email"]); ?></td>
				<td><?php echo utf8_encode($row["telefono"]); ?></td>
				<td><?php echo utf8_encode($row["asunto"]); ?></td>
				<td><?php echo utf8_encode($row["mensaje"]); ?></td>
				<td><?php echo fecha_reserva($row["fecha_contacto"]); ?></td>
			</tr>
		<?php
		}
		?>
			<tr>
				<td colspan="7" class="totales">Total contactos: <?php echo $total; ?></td>
			</tr>
		<?php
		mysql_close($link);
		?>
		</tbody>
		</table>
	</div>
</div>
</body>

</html>
