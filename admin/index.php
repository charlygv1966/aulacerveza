<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<h1><a href="index.php"><img src="img/logo_aulacerveza.png" alt="" /></a></h1>
		</div>
	</div>

	<div id="main-content">
		<h1>Gestor de Contenidos</h1>
		<div id="registrogestor">
			<form method="post" action="acceso.php" id="formu1ario" accept-charset="utf-8">
				<p>
				<label for="usuario_administracion">Usuario:</label>
				  <input type="text" name="usuario_administracion" id="usuario_administracion" placeholder="Introduce tu usuario" />
				</p>
				<p>
				<label for="password">Contraseña:</label>
				  <input type="password" name="password" id="password" placeholder="Introduce tu password" />
				</p>
				<button type="submit" id="enviar" name="enviar" value="Enviar">Enviar</button>
				<div class="mensajesregistro">
					<p></p>
				</div>
				<?php
				if (isset($_POST["msg"])) { ?>
					<div class="mensajegestor"><?php echo $_POST["msg"]; ?></div>
				<?php
				}
				?>
			</form>
		</div>
    <h2>© Carlos Garrido 2014-2022. Todos los derechos reservados.</h2>
	</div>
</div>
</body>

</html>
