			<div id="menudeopciones">
				<h2><a href="#chapter0">Configuración general</a></h2>
				<div>
					<ul>
						<li><a href="secciones.php">Secciones de la web</a></li>
						<li><a href="secciones-orden.php">Reordenar las secciones</a></li>
					</ul>
				</div>
				<h2><a href="#chapter1">Home</a></h2>
				<div>
					<ul>
						<li><a href="home-textos-izquierda.php">Editar texto banner izquierda</a></li>
						<li><a href="home-imagen-izquierda.php">Editar imagen banner izquierda</a></li>
						<li><a href="home-textos-central.php">Editar texto banner central</a></li>
						<li><a href="home-imagen-central.php">Editar imagen banner central</a></li>
						<li><a href="home-textos-derecha.php">Editar texto banner derecha</a></li>
						<li><a href="home-imagen-derecha.php">Editar imagen banner derecha</a></li>
					</ul>
				</div>
				<h2><a href="#chapter2">Quiénes Somos</a></h2>
				<div>
					<ul>
						<li><a href="quienes-somos-textos.php">Editar/modificar textos</a></li>
						<li><a href="quienes-somos-imagen.php">Editar/modificar imagen sección</a></li>
					</ul>
				</div>
				<h2><a href="#chapter3">Cursos</a></h2>
				<div>
					<ul>
						<li><a href="nuevo-curso.php">Nuevo curso</a></li>
						<li><a href="cursos-textos.php">Editar/modificar curso</a></li>
						<li><a href="cursos-reservas.php">Listado de reservas por curso</a></li>
						<li><a href="configuracion-cursos.php">Nº de cursos por página</a></li>
					</ul>
				</div>
				<h2><a href="#chapter4">Recursos</a></h2>
				<div>
					<ul>
						<li><a href="nuevo-recurso.php">Nuevo recurso</a></li>
						<li><a href="recursos-textos.php">Editar/modificar recurso</a></li>
						<li><a href="recursos-imagen.php">Editar/modificar imagen sección</a></li>
						<li><a href="configuracion-recursos.php">Nº de recursos por página</a></li>
					</ul>
				</div>
				<h2><a href="#chapter5">Foros</a></h2>
				<div>
					<ul>
						<li><a href="nuevo-foro.php">Nuevo Foro</a></li>
						<li><a href="foros-textos.php">Editar/modificar foro</a></li>
						<li><a href="foros-textos-comentarios.php">Editar/modificar comentarios de foro</a></li>
						<li><a href="foros-imagen.php">Editar/modificar imagen sección</a></li>
						<li><a href="configuracion-foros.php">Nº de foros por página</a></li>
						<li><a href="configuracion-foros-comentarios.php">Nº de comentarios de foros por página</a></li>
					</ul>
				</div>
				<h2><a href="#chapter6">Blog</a></h2>
				<div>
					<ul>
						<li><a href="nuevo-blog.php">Nuevo blog</a></li>
						<li><a href="blog-textos.php">Editar/modificar blog</a></li>
						<li><a href="configuracion-blog.php">Nº de blogs por página</a></li>
					</ul>
				</div>
				<h2><a href="#chapter7">Videos</a></h2>
				<div>
					<ul>
						<li><a href="videos-textos.php">Editar/modificar textos</a></li>
						<li><a href="videos-imagen.php">Editar/modificar imagen sección</a></li>
					</ul>
				</div>
				<h2><a href="#chapter8">Recetas</a></h2>
				<div>
					<ul>
						<li><a href="nueva-receta.php">Nueva receta</a></li>
						<li><a href="recetas-textos.php">Editar/modificar recetas</a></li>
						<li><a href="recetas-imagen.php">Editar/modificar imagen sección</a></li>
						<li><a href="configuracion-recetas.php">Nº de recetas por página</a></li>
					</ul>
				</div>
				<h2><a href="#chapter9">Fórmulas</a></h2>
				<div>
					<ul>
						<li><a href="formulas-textos.php">Editar/modificar textos</a></li>
						<li><a href="formulas-imagen.php">Editar/modificar imagen sección</a></li>
					</ul>
				</div>
				<h2><a href="#chapter10">Contacto</a></h2>
				<div>
					<ul>
						<li><a href="contacto-textos.php">Editar/modificar textos</a></li>
						<li><a href="contacto-imagen.php">Editar/modificar imagen sección</a></li>
					</ul>
				</div>
				<h2><a href="#chapter10">Listado de contactos</a></h2>
				<div>
					<ul>
						<li><a href="ver-contactos.php">Ver contactos</a></li>
					</ul>
				</div>
				<h2><a href="#chapter11">Listado de usuarios registrados</a></h2>
				<div>
					<ul>
						<li><a href="ver-usuarios.php">Ver usuarios</a></li>
					</ul>
				</div>
			</div>
