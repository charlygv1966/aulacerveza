<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>RECURSOS (Imágenes)</h1>
		<h2><?php echo fecha(); ?></h2>
		<h3>Si no quieres que el artículo seleccionado tenga una imagen asociada pero actualmente la tiene, haz clic sobre la misma y será eliminada.<br />(<em>Te recuerdo que no es necesario que un artículo lleve imagen)</em></h3>
		<form method="post" action="recurso-imagen-modificar.php" accept-charset="utf-8" enctype="multipart/form-data">
			<?php
			$query="SELECT id,titulo,imagen FROM recursos WHERE id=".$_GET["id_recurso"];
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
			<h4>Recurso: <strong><?php echo utf8_encode($row["titulo"]); ?></strong></h4>
			<div class="contacto-imagen">
				<?php if ($row["imagen"]) { ?>
				<p><a title="Eliminar imagen <?php echo $row["imagen"]; ?>" href="recurso-imagen-eliminar.php?id_recurso=<?php echo $_GET["id_recurso"]; ?>"><img class="eliminar" src="../images/recursos/secciones/<?php echo $row["imagen"]; ?>"></a>&nbsp;Nombre: <strong><?php echo $row["imagen"]; ?></strong></p>
				<?php } else { ?>
				<p>El artículo seleccionado no tiene imagen asociada</p>
				<?php } ?>
				<br />
				<p>Selecciona la nueva imagen para la sección. (Tamaño recomendado: 280px por 265px)</p>
			</div>
			<input type="hidden" name="id_recurso" id="id_recurso" value="<?php echo $row["id"]; ?>" />
			<input type="file" name="imagen" id="imagen" /><br />
			<button type="reset" id="cancelarrecursos" name="cancelarrecursos">VOLVER</button>
			<button type="button" id="recursoimagen" name="recursoimagen">AÑADIR o MODIFICAR IMAGEN</button>
			<?php
			}
			?>
		</form>
		<div class="mensajes">
			<?php if ($_GET["mensaje"] == "ok") {
				echo "<p>Has modificado la imagen de la sección</p>";
			} elseif ($_GET["mensaje"] == "error") {
				echo "<p>Se ha producido un error</p>";
			} else {
				echo "<p>Para una correcta visualización la imagen debe ser de 280px por 265px</p>";
			}
			?>
		</div>
	</div>
</div>
</body>

</html>
