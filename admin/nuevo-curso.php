<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");

	if ($_POST) {
		$cursotitulo = $_POST["cursotitulo"];
		$cursodescripcion = $_POST["cursodescripcion"];
		$cursolugar = $_POST["cursolugar"];
		$cursoplazas = $_POST["cursoplazas"];
		$cursoforma_pago = $_POST["cursoforma_pago"];
		$cursotipo = $_POST["cursotipo"];
		$cursoprecio = $_POST["cursoprecio"];
		$cursocomienzo = $_POST["cursocomienzo"];
		$cursofinal = $_POST["cursofinal"];
		$cursopublicado = $_POST["cursopublicado"];
		$msg=$_POST["msg"];
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>CURSOS (Nuevo curso)</h1>
		<h2><?php echo fecha(); ?></h2>
		<form method="post" accept-charset="utf-8" action="nuevo-curso-actualizar.php">
			<p>
				<label for="cursotitulo">Nombre del curso</label>
				<input type="text" name="cursotitulo" id="cursotitulo" placeholder="Introduce el nombre del curso" value="<?php echo $cursotitulo;; ?>" />
			</p>
			<p>
				<label for="cursodescripcion">Descripción</label>
				<textarea id="cursodescripcion" name="cursodescripcion"><?php echo $cursodescripcion; ?></textarea>
			</p>
			<br />
			<p>
				<label for="cursolugar">Lugar de realización</label>
				<textarea name="cursolugar" id="cursolugar"><?php echo $cursolugar; ?></textarea>
			</p>
			<br />
			<p>
				<label for="cursoplazas">Número de plazas</label>
				<input type="text" name="cursoplazas" id="cursoplazas" placeholder="Introduce el número de plazas del curso" value="<?php echo $cursoplazas; ?>" class="tipocurso" />
			</p>
			<p>
				<label for="cursoforma_pago">Forma de pago</label>
				<input type="text" name="cursoforma_pago" id="cursoforma_pago" value="PAGO ÚNICO" class="tipocurso" readonly="readonly" />
			</p>
			<p>
				<label for="cursotipo">Tipo del curso</label>
				<select name="cursotipo" id="cursotipo" class="tipocurso">
					<option value="">Selecciona el tipo de curso</option>
					<option value="PRESENCIAL" <?php if ($cursotipo == "PRESENCIAL") { echo "selected=\"selected\""; } ?>>PRESENCIAL</option>
					<option value="ONLINE" <?php if ($cursotipo == "ONLINE") { echo "selected=\"selected\""; } ?>>ONLINE</option>
				</select>
			</p>
			<p>
				<label for="cursoprecio">Precio del curso</label>
				<input type="text" name="cursoprecio" id="cursoprecio" placeholder="Introduce el precio del curso" value="<?php echo visualizacion_precios($cursoprecio); ?>" class="tipocurso" /> &euro;
			</p>
			<p>
				<label for="cursocomienzo">Fecha de comienzo del curso</label>
				<input type="text" name="cursocomienzo" id="cursocomienzo" placeholder="Introduce la fecha de inicio del curso" value="<?php echo $cursocomienzo; ?>" class="tipocurso" />
			</p>
			<p>
				<label for="cursofinal">Fecha de finalización del curso</label>
				<input type="text" name="cursofinal" id="cursofinal" placeholder="Introduce la fecha de finalización del curso" value="<?php echo $cursofinal; ?>" class="tipocurso" />
			</p>
			<p>
				<label for="cursopublicado">Publicado</label>
				<select name="cursopublicado" id="cursopublicado">
					<option value="si" <?php if ($cursopublicado == "si") { echo "selected=\"selected\""; } ?>>Si</option>
					<option value="no" <?php if ($cursopublicado == "no") { echo "selected=\"selected\""; } ?>>No</option>
				</select>
			</p>
			<p>
				<button type="reset" id="cancelar" name="cancelar">VOLVER</button>
				<button type="submit" id="cursos" name="cursos">ACEPTAR</button>
			</p>
			<div class="separator"></div>
			<p>
				Si el curso que has creado es correcto se grabará y te redireccionaremos a la pantalla en la que deberás añadir una imagen para su correcta visualización.
			</p>
		</form>

		<div id="mensajes" class="mensajes">
		<?php
			$mensajeenviado = $_GET["mensaje"];
			switch ($mensajeenviado) {
				case '2':
					$mensaje = "Has actualizado el contenido";
					break;
				case '3':
					$mensaje = "Se ha producido un error";
					break;
				default:
					$mensaje = "";
					break;
			}
			echo $mensaje;
			if ($msg=="" && (!isset($_GET["mensaje"]))) {
				$msg = "Debes incluir correctamente todos los campos";
			}
			echo $msg;
		?>
		</div>

	</div>
</div>
</body>

</html>
