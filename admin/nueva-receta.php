<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");

	if ($_POST) {
		$recetatitulo = $_POST["recetatitulo"];
		$recetatextos = $_POST["recetatextos"];
		$recetapublicado = $_POST["recetapublicado"];
		$msg=$_POST["msg"];
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>RECETAS (Nueva receta)</h1>
		<h2><?php echo fecha(); ?></h2>
		<form method="post" accept-charset="utf-8" action="nueva-receta-actualizar.php">
			<p>
				<label for="recetatitulo">Título</label>
				<input type="text" name="recetatitulo" id="recetatitulo" placeholder="Introduce el título de la receta" value="<?php echo $recetatitulo; ?>" />
			</p>
			<p>
				<label for="recetatextos">Texto del artículo</label>
				<textarea id="recetatextos" name="recetatextos"><?php echo $recetatextos; ?></textarea>
			</p>
			<br />
			<p>
				<label for="recetapublicado">Publicado</label>
				<select name="recetapublicado" id="recetapublicado">
					<option value="si" <?php if ($recetapublicado == "si") { echo "selected=\"selected\""; } ?>>Si</option>
					<option value="no" <?php if ($recetapublicado == "no") { echo "selected=\"selected\""; } ?>>No</option>
				</select>
			</p>
			<p>
				<button type="reset" id="cancelar" name="cancelar">VOLVER</button>
				<button type="submit" id="recetas" name="recetas">ACEPTAR</button>
			</p>
			<div class="separator"></div>
			<p>
				Si la receta que has creado es correcta se grabará y te redireccionaremos a la pantalla en la que deberás añadir (opcionalmente) una imagen.
			</p>
		</form>

		<div class="mensajes">
		<?php
			$mensajeenviado = $_GET["mensaje"];
			switch ($mensajeenviado) {
				case '2':
					$mensaje = "Has actualizado el contenido";
					break;
				case '3':
					$mensaje = "Se ha producido un error";
					break;
				default:
					$mensaje = "";
					break;
			}
			echo $mensaje;
			if ($msg=="" && (!isset($_GET["mensaje"]))) {
				$msg = "Debes incluir correctamente todos los campos";
			}
			echo $msg;
		?>
		</div>

	</div>
</div>
</body>

</html>
