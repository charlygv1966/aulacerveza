<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<section id="galeria">
			<?php
			$query="SELECT titulo_izquierda, texto_izquierda, imagen_izquierda FROM home";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
			<article class="izquierda">
				<figure>
					<img src="images/home/<?php echo $row["imagen_izquierda"]; ?>" alt="<?php echo utf8_encode($row["titulo_izquierda"]); ?>" />
					<figcaption></figcaption>
				</figure>
				<div class="homedestacado">
					<h2><?php echo utf8_encode($row["titulo_izquierda"]); ?></h2>
					<?php echo $row["texto_izquierda"]; ?>
				</div>
			</article>
			<?php
			}
			?>
			<?php
			$query="SELECT titulo_central, texto_central, imagen_central FROM home";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
			<article class="centro">
				<figure>
					<img src="images/home/<?php echo $row["imagen_central"]; ?>" alt="<?php echo utf8_encode($row["titulo_central"]); ?>" />
					<figcaption></figcaption>
				</figure>
				<div class="homedestacado">
					<h2><?php echo utf8_encode($row["titulo_central"]); ?></h2>
					<?php echo $row["texto_central"]; ?>
				</div>
			</article>
			<?php
			}
			?>
			<?php
			$query="SELECT titulo_derecha, texto_derecha, imagen_derecha FROM home";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
			<article class="derecha">
				<figure>
					<img src="images/home/<?php echo $row["imagen_derecha"]; ?>" alt="<?php echo utf8_encode($row["titulo_derecha"]); ?>" />
					<figcaption></figcaption>
				</figure>
				<div class="homedestacado">
					<h2><?php echo utf8_encode($row["titulo_derecha"]); ?></h2>
					<?php echo $row["texto_derecha"]; ?>
				</div>
			</article>
			<?php
			}
			?>
		</section>
	</section>
</div>

</body>
</html>

