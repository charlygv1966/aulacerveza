<?php
	session_start();
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>BIENVENIDO AL GESTOR DE CONTENIDOS (<?php echo $_SESSION["usuario_administracion"]; ?>)</h1>
		<h2><?php echo fecha(); ?></h2>
		<div id="registrogestor">
			<figure>
				<img src="img/web.jpg" alt="AULA CERVEZA" longdesc="AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman" />
				<figcaption>AULA CERVEZA</figcaption>
			</figure>
			<h2>En la parte izquierda de la pantalla hay un menu desplegable que permite acceder a todos los contenidos de la web.</h2>
			<h2>Puedes personalizar todos los textos e imágenes disponibles a través de este gestor.</h2>
			<h3><em>&copy; Carlos Garrido 2014-2015 Todos los derechos reservados</em></h3>
		</div>
	</div>
</div>
</body>

</html>
