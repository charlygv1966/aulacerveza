<?php
session_start();
header('Content-Type: text/html; charset=UTF-8');
include("admin/inc/conexion.php");
include("inc/funciones.php");
if ($_POST) {
	$nombre=$_POST["nombre"];
	$apellidos=$_POST["apellidos"];
	$email=$_POST["email"];
	$pais=$_POST["pais"];
	//contraseña actual
	$password=$_POST["password"];
	//nueva contraseña
	$password2=$_POST["password2"];
	$password3=$_POST["password3"];
	$msg=$_POST["msg"];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Contacto</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/jquery-ui-1.11.1.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-ui-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="registrogeneralmodificar">
			<h2><img src="images/registro/registro.png" alt="Registro"></h2>
			<h3>Modificación de datos</h3>
			<?php
			$query="SELECT * FROM usuarios WHERE id=".$_SESSION["id_usuario"];
			$result=mysql_query($query);
			if ($row=mysql_fetch_array($result)) {
			?>
			<form method="post" id="formu1ario" accept-charset="utf-8" action="modifica.php">
				<p>
				<label for="nombre">Nombre: *</label>
				<input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre" value="<?php echo utf8_encode($row["nombre"]); ?>" />
				</p>
				<p>
				<label for="apellidos">Apellidos: *</label>
				<input type="text" name="apellidos" id="apellidos" placeholder="Introduce tus apellidos" value="<?php echo utf8_encode($row["apellidos"]); ?>" />
				</p>
				<p>
				<label for="email">E-Mail: <span>(no modificable)</span></label>
				<input type="text" name="email" id="email" placeholder="Introduce tu e-mail" value="<?php echo utf8_encode($row["email"]); ?>" readonly="readonly" />
				</p>
				<p>
				<label for="pais">País: *</label>
				<input type="text" name="pais" id="pais" placeholder="Introduce tu país" value="<?php echo utf8_encode($row["pais"]); ?>" />
				</p>
				<p>
				<label for="password">Contraseña actual: *</label>
				<input type="password" name="password" id="password" placeholder="Introduce tu password actual" />
				</p>
				<p>
				<label for="password2">Nueva contraseña: *</label>
				<input type="password" name="password2" id="password2" placeholder="Introduce tu nueva password" />
				</p>
				<p>
				<label for="password3">Verificar nueva contraseña: *</label>
				<input type="password" name="password3" id="password3" placeholder="Introduce tu nueva password otra vez" />
				</p>
				<p class="requeridos">Campos requeridos*</p>
				<div class="botonera">
					<button type="button" id="cancelarmodi" name="cancelarmodi" value="Cancelar">Cancelar</button>
					<button type="submit" id="enviar" name="enviar" value="Modificar">Modificar</button>
				</div>
				<div class="mensajesregistro">
					<?php include("inc/mensajes-registro-modificar.php"); ?>
				</div>
			</form>
			<?php
			}
			?>
		</section>
	</section>
</div>

</body>
</html>

