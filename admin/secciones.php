<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>SECCIONES PRINCIPALES DE LA WEB</h1>
		<h2><?php echo fecha(); ?></h2>
		<h3>SECCIONES</h3>
		<h3>Puedes despublicar cualquier sección para que no se vea en la web. En cuanto a los <strong>CURSOS</strong>, puedes despublicarlos todos, solo los <em>PRESENCIALES</em> o solo los <em>ONLINE</em>.</h3>
		<h3>Si quieres despublicar un curso en particular entra en la sección <strong>Cursos - Editar/Modificar curso</strong> y accede <strong>Editar/Modificar</strong> marcando la opción de <strong>Publicado (Si/No)</strong></h3>
		<table summary="Listado de secciones publicadas" class="contactos">
		<thead>
			<tr>
				<th>Sección</th>
				<th class="centrado">Publicado</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$query="SELECT * FROM menu ORDER BY clase, id";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
			<tr>
				<!--<td><?php echo utf8_encode($row["nombre"]); ?></td>-->
				<td>
					<?php
						if ($row["espadre"] == 1) {
							 echo utf8_encode($row["nombre"]);
						} else {
							 echo "&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&nbsp;".utf8_encode($row["nombre"]);
						}
					?>
				</td>
				<td class="centrado">
				<select name="publicado" id="publicado" disabled="disabled">
					<option value="1" <?php if ($row["activo"] == 1) { echo "selected=\"selected\""; } ?>>Si</option>
					<option value="0" <?php if ($row["activo"] == 0) { echo "selected=\"selected\""; } ?>>No</option>
				</select>
				</td>
				<td class="centrado"><a href="secciones-modificar.php?seccion=<?php echo $row["id"]; ?>">Publicar/Despublicar</a></td>
			</tr>
			<?php
			}
			?>
		</tbody>
		</table>
		<button type="reset" id="cancelar" name="cancelar">VOLVER</button>

		<div class="mensajes">
			<?php if ($_GET["mensaje"] == "2") {
				echo "<p>Has modificado la configuración de las opciones del menu</p>";
			} elseif ($_GET["mensaje"] == "3") {
				echo "<p>Se ha producido un error</p>";
			} else {
				echo "<p>&nbsp;</p>";
			}
			?>
		</div>
	</div>
</div>
</body>

</html>
