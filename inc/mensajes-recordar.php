<?php
	$mensajeenviado = $_GET["mensaje"];
	switch ($mensajeenviado) {
		case '1':
			$mensaje = "El usuario que has indicado no está actualmente registrado en la web";
			break;
		case '2':
			$mensaje = "<strong>Te hemos enviado al correo que nos has indicado tu contraseña</strong><br /><br />Si no la has recibido ponte en contacto con nosotros a través de los datos de contacto de la web";
			break;
		case '3':
			$mensaje = "<strong>Se ha producido un error</strong><br /><br />&iexcl;Inténtalo más tarde!";
			break;
		default:
			$mensaje = "";
			break;
	}
	echo $mensaje;

	if ($msg=="" && (!isset($_GET["mensaje"]))) {
		$msg = "Debes incluir correctamente todos los campos";
	}
	echo $msg;
?>