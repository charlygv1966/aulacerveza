<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");

	if ($_POST) {
		$configuracion_foros_comentarios = $_POST["configuracion_foros_comentarios"];
		$msg=$_POST["msg"];
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>CONFIGURACIÓN GENERAL DE OPCIONES (Número de comentarios de foro por página)</h1>
		<h2><?php echo fecha(); ?></h2>
		<h3>ESTA PÁGINA DEL GESTOR PERMITE CONFIGURAR EL NÚMERO DE COMENTARIOS POR FORO QUE SE MUESTRAN POR PÁGINA, PERMITIENDO NAVEGAR ENTRE LOS MISMOS MEDIANTE LOS ENLACES DE <strong>PRIMÉRA PÁGINA</strong>, <strong>ANTERIOR</strong>, <strong>NÚMERO DE PÁGINA (con intervalo de 5 enlaces por página)</strong>, <strong>SIGUIENTE</strong> y <strong>ÚLTIMA PÁGINA</strong></h3>
		<h3><strong>NOTA:</strong> ESTA CONFIGURACIÓN ES PARA LA PÁGINA DE COMENTARIOS DE FORO PÚBLICA NO PARA EL GESTOR DE CONTENIDOS</h3>
		<form method="post" action="configuracion-foros-comentarios-modificar.php" accept-charset="utf-8">
			<?php
			$query="SELECT foros_comentarios FROM configuracion_general";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
				<p>
					<label for="configuracion_foros_comentarios">Número de comentarios de foro por página</label>
					<input type="text" name="configuracion_foros_comentarios" id="configuracion_foros_comentarios" placeholder="Introduce la cantidad de registros por página" value="<?php if ($row) { echo $row["foros_comentarios"]; } else { echo $configuracion_foros_comentarios; } ?>" />
				</p>
				<button type="reset" id="cancelar" name="cancelar">VOLVER</button>
				<button type="submit" id="contacto" name="contacto">MODIFICAR</button>
			<?php
			}
			?>
		</form>
		<div class="mensajes">
		<?php
			$mensajeenviado = $_GET["mensaje"];
			switch ($mensajeenviado) {
				case '2':
					$mensaje = "Has actualizado el contenido";
					break;
				case '3':
					$mensaje = "Se ha producido un error";
					break;
				default:
					$mensaje = "";
					break;
			}
			echo $mensaje;
			if ($msg=="" && (!isset($_GET["mensaje"]))) {
				$msg = "Debes incluir correctamente todos los campos";
			}
			echo $msg;
		?>
		</div>
	</div>
</div>
</body>

</html>
