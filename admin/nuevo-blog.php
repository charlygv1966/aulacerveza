<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");

	if ($_POST) {
		$blogtitulo = $_POST["blogtitulo"];
		$blogtextos = $_POST["blogtextos"];
		$blogpublicado = $_POST["blogpublicado"];
		$msg=$_POST["msg"];
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>BLOG (Nuevo blog)</h1>
		<h2><?php echo fecha(); ?></h2>
		<form method="post" accept-charset="utf-8" action="nuevo-blog-actualizar.php">
			<p>
				<label for="blogtitulo">Título</label>
				<input type="text" name="blogtitulo" id="blogtitulo" placeholder="Introduce el título del blog" value="<?php echo $blogtitulo; ?>" />
			</p>
			<p>
				<label for="blogtextos">Texto del artículo</label>
				<textarea id="blogtextos" name="blogtextos"><?php echo $blogtextos; ?></textarea>
			</p>
			<br />
			<p>
				<label for="blogpublicado">Publicado</label>
				<select name="blogpublicado" id="blogpublicado">
					<option value="si" <?php if ($blogpublicado == "si") { echo "selected=\"selected\""; } ?>>Si</option>
					<option value="no" <?php if ($blogpublicado == "no") { echo "selected=\"selected\""; } ?>>No</option>
				</select>
			</p>
			<p>
				<button type="reset" id="cancelar" name="cancelar">VOLVER</button>
				<button type="submit" id="blogs" name="blogs">ACEPTAR</button>
			</p>
			<div class="separator"></div>
			<p>
				Si el blog que has creado es correcto se grabará y te redireccionaremos a la pantalla en la que deberás añadir (opcionalmente) una imagen.
			</p>
		</form>

		<div class="mensajes">
		<?php
			$mensajeenviado = $_GET["mensaje"];
			switch ($mensajeenviado) {
				case '2':
					$mensaje = "Has actualizado el contenido";
					break;
				case '3':
					$mensaje = "Se ha producido un error";
					break;
				default:
					$mensaje = "";
					break;
			}
			echo $mensaje;
			if ($msg=="" && (!isset($_GET["mensaje"]))) {
				$msg = "Debes incluir correctamente todos los campos";
			}
			echo $msg;
		?>
		</div>

	</div>
</div>
</body>

</html>
