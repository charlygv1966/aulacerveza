<?php
header('Content-Type: text/html; charset=UTF-8');
include("admin/inc/conexion.php");
include("inc/funciones.php");
require ("mail/class.phpmailer.php");
$lang = "español";
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Contacto</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<?php
$email=trim($_POST["email"]);
$envio=1;

if(esVacio($email)){
	$envio=0;
	$msg="Introduce tu email";
} else if(!esVacio($email) && !esEmail($email)){
	$envio=0;
	$msg="Introduce un email v&aacute;lido";
}

//si envio OK
if ($envio) {
	$query="SELECT * FROM usuarios WHERE email='".$email."'";
	$result=mysql_query($query);

	if ($row=mysql_fetch_array($result)) {
		$nombre = $row["nombre"];
		$apellidos = $row["apellidos"];
		$email = $row["email"];
		$password = md5($row["password"]);

		try {
			$mail = new PHPMailer(true); //New instance, with exceptions enabled

			$body="AULA CERVEZA. Recordar contraseña:"."\n\n";
			$body.="Hola ".$nombre."\n\n";
			$body.="Los datos de tu usuario son los siguientes:\n\n";
			$body.="Nombre: ".$nombre."\n";
			$body.="Apellidos: ".$apellidos."\n";
			$body.="Email: ".$email."\n";
			$body.="Password: ".$password."\n";
			$body=utf8_decode($body);

			$mail->SetLanguage('es');
			$mail->IsSMTP();                           			// tell the class to use SMTP
			$mail->SMTPAuth   = true;                 			// enable SMTP authentication
			$mail->Port       = 587;                    		// set the SMTP server port
			$mail->Host       = "127.0.0.1"; 					// SMTP server
			$mail->Username   = "contacto@aulacerveza.com";     // SMTP server username
			$mail->Password   = "password";            			// SMTP server password
			$mail->From       = "contacto@aulacerveza.com";
			$mail->FromName   = "Aula Cerveza";
			$to = $email;
			$mail->AddAddress($to);
			$mail->Subject    = "Solicitud de recordatorio de contraseña de Aula Cerveza";
			$mail->AltBody    = ""; // optional, comment out and test
			$mail->WordWrap   = 80; // set word wrap
			$mail->MsgHTML($body);
			$mail->IsHTML(true); // send as HTML
			$mail->Send(); ?>
			<script type="text/javascript">
				document.location.href="recordar.php?mensaje=2";
			</script>
			<?php
		} catch (phpmailerException $e) { ?>
			<script type="text/javascript">
				document.location.href="recordar.php?mensaje=3";
			</script>
			<?php
		}
	} else {
	//el usuario no está registrado en la web
	?>
	<script type="text/javascript">
		document.location.href="recordar.php?mensaje=1";
	</script>
	<?php
	}
} else {
?>
<form method="post" action="recordar.php" id="form" name="form" accept-charset="utf-8">
<input type="hidden" name="email" value="<?php echo $email; ?>" />
<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
</form>
<script type="text/javascript">
	document.form.submit();
</script>
<?php } ?>

</body>
</html>