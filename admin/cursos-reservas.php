<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>CURSOS (Reservas de cursos)</h1>
		<h2><?php echo fecha(); ?></h2>
		<h3>CURSOS PUBLICADOS</h3>
		<table summary="Listado de cursos publicados" class="contactos">
		<thead>
			<tr>
				<th>Título</th>
				<th>Tipo</th>
				<th>Reservas</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$query="SELECT * FROM cursos ORDER BY id";
			$result=mysql_query($query);
			$total = mysql_num_rows($result);
			while ($row=mysql_fetch_array($result)) {
			?>
			<tr>
				<td><?php echo utf8_encode($row["titulo"]); ?></td>
				<td><?php echo utf8_encode($row["tipo_curso"]); ?></td>
				<td class="centrado"><a href="cursos-reservas-ver.php?curso=<?php echo $row["id"]; ?>">Ver reservas</a></td>
			</tr>
			<?php
			}
			?>
			<tr>
				<td colspan="3" class="totales">Total cursos: <?php echo $total; ?></td>
			</tr>
		</tbody>
		</table>
		<button type="reset" id="cancelar" name="cancelar">VOLVER</button>
	</div>
</div>
</body>

</html>
