<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	//excluir este fichero de este documento
	//include("inc/funciones.php");

	function normalizacion($txt){
		$texto = array("á","é","í","ó","ú","ñ","ü","Á","É","Í","Ó","Ú","Ñ","Ü"," ","%20","'");
		$textoreplace = array("a","e","i","o","u","n","u","a","e","i","o","u","n","u","-","-","-");
		return strtolower(str_replace($texto,$textoreplace,$txt));
	}

	if (!is_uploaded_file($_FILES['imagen']['tmp_name'])) {
			header("Location:error.php");
	} else {
		//capturo el nombre del fichero subido
		$imagen = $_FILES['imagen']['name'];
		//lo parto en trozos por si hay algun punto (.) de más en el nombre
		$imagen1 = explode(".",$imagen);
		//veo los trozos en los que se ha partido el nombre, es decir, el tamaño resultante del array
		$tamanio_array = count($imagen1);
		//el último trozo tiene que ser la extension del fichero
		$extension_fichero = strtolower($imagen1[$tamanio_array-1]);
		//creo una variable para guardar el nombre normalizado
		$imagen2 = "";
		//normalizo todos los trozos menos el último separandolos por guiones altos (-)
		for ($i=0;$i<$tamanio_array-1;$i++) {
			$imagen2.=normalizacion($imagen1[$i])."-";
		}
		//borro el último caracter resultante (-) antes de añadirle al fichero la extensión
		$imagen2 = substr($imagen2,0,strlen($imagen2)-1);
		//al fichero normalizado le añado un punto (.), un número aleatorio y luego la extensión guardada anteriormente
		$imagen2.=".".$extension_fichero;
		//ya tengo el fichero normalizado. procedo a subirlo
		try {
			move_uploaded_file($_FILES['imagen']['tmp_name'], "../images/recetas/".$imagen2);
		} catch (Exception $e) {print_r($e);}
		//Asigno a la foto permisos
		$ruta="../images/recetas/".$imagen2;
		chmod($ruta,0777);
		//seleccionamos el fichero actual...
		$query = "SELECT imagen FROM recetas_imagen";
		$result=mysql_query($query);
		while ($row=mysql_fetch_array($result)) {
			$imagen_actual = $row["imagen"];
		}

		//... y lo borramos
		if ($imagen_actual != $imagen2) {
			unlink("../images/recetas/".$imagen_actual);
		}

		//luego subimos la nueva imagen
		$query2 = "UPDATE recetas_imagen SET imagen='$imagen2'";
		$result2=mysql_query($query2);
		mysql_close($link);
		if ($result2){
		?>
		<script type="text/javascript">
			document.location.href="recetas-imagen.php?mensaje=ok";
		</script>
		<?php
		} else {
		?>
		<script type="text/javascript">
			document.location.href="recetas-imagen.php?mensaje=error";
		</script>
		<?php
		}
	}
?>
