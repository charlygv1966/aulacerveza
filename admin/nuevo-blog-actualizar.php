<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>BLOG (Textos)</h1>
		<?php
		$blogtitulo = trim($_POST["blogtitulo"]);
		$blogtextos = trim($_POST["blogtextos"]);
		$blogpublicado = trim($_POST["blogpublicado"]);
		$envio=1;

		if (esVacio($blogtitulo)) {
			$envio=0;
			$msg="Introduce el título del blog";
		} else if(esVacio($blogtextos)){
			$envio=0;
			$msg="Introduce el contenido del blog";
		} else if(esVacio($blogpublicado)){
			$envio=0;
			$msg="Debes indicar si quieres publicar o no el blog";
		}

		if ($envio) {
			$query = "INSERT INTO blog (titulo, textos, publicado, fecha_publicacion) VALUES ('". utf8_decode(normalizacion($blogtitulo))."','".utf8_decode($blogtextos)."','".utf8_decode(normalizacion($blogpublicado))."', now())";
			$result=mysql_query($query);
			$id_blog = mysql_insert_id($link);
			mysql_close($link);
				if ($result){
				?>
				<script type="text/javascript">
					document.location.href="blog-imagen-actualizar.php?id_blog=<?php echo $id_blog; ?>&amp;mensaje=2";
				</script>
				<?php
				} else {
				?>
				<script type="text/javascript">
					document.location.href="nuevo-blog.php?mensaje=3";
				</script>
				<?php
				}
		} else {
		?>
		<form method="post" action="nuevo-blog.php" id="form" name="form" accept-charset="utf-8">
		<input type="hidden" name="blogtitulo" value="<?php echo $blogtitulo; ?>" />
		<input type="hidden" name="blogtextos" value="<?php echo $blogtextos; ?>" />
		<input type="hidden" name="blogpublicado" value="<?php echo $blogpublicado; ?>" />
		<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
		</form>
		<script type="text/javascript">
			document.form.submit();
		</script>
		<?php
		}
		?>
	</div>
</div>
</body>

</html>
