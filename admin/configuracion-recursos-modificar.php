<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>RECURSOS</h1>
		<?php
		$configuracion_recursos = trim($_POST["configuracion_recursos"]);
		$envio=1;

		if (esVacio($configuracion_recursos)) {
			$envio=0;
			$msg="Debes indicar un número";
		}

		if ($envio) {
			$query = "UPDATE configuracion_general SET recursos='$configuracion_recursos'";
			$result=mysql_query($query);
			mysql_close($link);
				if ($result){
				?>
				<script type="text/javascript">
					document.location.href="configuracion-recursos.php?mensaje=2";
				</script>
				<?php
				} else {
				?>
				<script type="text/javascript">
					document.location.href="configuracion-recursos.php?mensaje=3";
				</script>
				<?php
				}
		} else {
		?>
		<form method="post" action="configuracion-recursos.php" id="form" name="form" accept-charset="utf-8">
		<input type="hidden" name="configuracion-recursos" value="<?php echo $configuracion-recursos; ?>" />
		<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
		</form>
		<script type="text/javascript">
			document.form.submit();
		</script>
		<?php
		}
		?>
	</div>
</div>
</body>

</html>
