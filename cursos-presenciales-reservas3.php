<?php
session_start();
header('Content-Type: text/html; charset=UTF-8');
include("inc/funciones.php");
if (!$_SESSION["entrar"] == "SI") {
	header("Location:cursos-presenciales.php");
}
$id_curso = $_GET["curso"];
$plazas_reservadas = $_POST["reservas"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Cursos presenciales</title>
<link rel="stylesheet" href="css/jquery-ui-1.11.1.css" />
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-ui-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="cursos">
			<div id="listado">
				<h2><img src="images/cursos/presenciales.png" alt="PRESENCIALES"></h2>
				<section id="curso">
					<?php
					$query="SELECT * FROM cursos WHERE id=".$id_curso." AND publicado='si'";
					$result=mysql_query($query);
					while ($row=mysql_fetch_array($result)) {
					?>
					<article>
						<figure>
							<img src="images/cursos/presencial/<?php echo $row["imagen"]; ?>" alt="<?php echo utf8_encode($row["titulo"]); ?>" />
							<figcaption><?php echo utf8_encode($row["titulo"]); ?></figcaption>
						</figure>
						<div class="textos reservas">
							<h2>Reserva del curso</h2>
							<ol>
								<li>1. Introducción de datos</li>
								<li>2. Confirmación de datos</li>
								<li class="sel">3. Finalización</li>
							</ol>
							<div class="caracteristicas-curso">
								<p>CURSO: <strong><?php echo utf8_encode($row["titulo"]); ?></strong></p>
								<?php echo $row["lugar_realizacion"]; ?>
								<p>Plazas: <strong><?php echo $row["plazas"]; ?></strong> - (Reservas: <?php echo $plazas_reservadas; ?>) - Total: <strong><?php echo $row["plazas"] - $plazas_reservadas; ?></strong></p>
								<p>Precio del curso: <strong><?php echo number_format($row["precio_curso"],2,',','.'); ?> €</strong></p>
								<p>Fecha: <strong><?php echo fecha_foro($row["fecha_comienzo"]); ?> - <?php echo fecha_foro($row["fecha_final"]); ?></strong></p>
								<p>Forma de pago: <strong><?php echo utf8_encode($row["forma_pago"]); ?></strong></p>
								<label for="reservado">Persona que se inscribe en este curso:</label>
								<form method="post" id="cursos-online-reservas" accept-charset="utf-8" action="cursos-presenciales-pagar.php">
									<input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $_SESSION["id_usuario"]; ?>" />
									<input type="hidden" name="id_curso" id="id_curso" value="<?php echo $id_curso; ?>" />
									<input type="text" name="persona_reserva" id="persona_reserva" value="<?php echo $_POST["persona_reserva"]; ?>" readonly="readonly" /><br />
									<input type="checkbox" name="condiciones" id="condiciones" checked="checked" disabled="disabled" /> He leido y acepto las <a href="#" id="condiciones-devolucion">condiciones de devolución</a><br />
									<button type="submit" id="pagar-reserva">PAGAR</button>
									<button type="button" id="cancelar-reserva-presenciales">CANCELAR</button>
								</form>
							</div>
						</div>
					</article>
					<?php
					}
					?>
				</section>
			</div>
		</section>
	</section>
</div>

</body>
</html>

