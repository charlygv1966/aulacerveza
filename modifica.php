<?php
session_start();
header('Content-Type: text/html; charset=UTF-8');
include("admin/inc/conexion.php");
include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Contacto</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
	<?php
	$nombre=trim(utf8_decode(normalizacion($_POST["nombre"])));
	$apellidos=trim(utf8_decode(normalizacion($_POST["apellidos"])));
	$email=trim($_POST["email"]);
	$pais=utf8_decode(normalizacion($_POST["pais"]));
	$password=$_POST["password"];
	$password2=$_POST["password2"];
	$password3=$_POST["password3"];
	$envio=1;

	if (esVacio($nombre)) {
		$envio=0;
		$msg="Introduce tu nombre";
	} else if (esVacio($apellidos)){
		$envio=0;
		$msg="Introduce tus apellidos";
	} else if(esVacio($email)){
		$envio=0;
		$msg="Introduce tu email";
	} else if(!esVacio($email) && !esEmail($email)){
		$envio=0;
		$msg="Introduce un email v&aacute;lido";
	} else if(esVacio($pais)){
		$envio=0;
		$msg="Introduce tu pais";
	} else if ($password==""){
		$envio=0;
		$msg="Debes indicar tu contraseña actual";
	} else if ($password2==""){
		$envio=0;
		$msg="Debes indicar tu nueva contraseña";
	} else if ($password3==""){
		$envio=0;
		$msg="Debes confirmar tu nueva contraseña una segunda vez";
	} else if ($password2!=$password3){
		$envio=0;
		$msg="Debes indicar la misma contraseña";
	}

	//si envio OK
	if ($envio) {
		$query="SELECT COUNT(*) AS cuantos FROM usuarios WHERE email='".$email."' AND password='".md5($password)."'";
		$result=mysql_query($query);
		$row=mysql_fetch_array($result);
		$cuantos=$row["cuantos"];
		if ($cuantos){
			$passwordnueva = md5($password2);
			$query = "UPDATE usuarios SET nombre='$nombre', apellidos='$apellidos', pais='$pais', password='$passwordnueva' WHERE email='".$email."'";
			$result2=mysql_query($query);
			if ($result2){ ?>
				<script type="text/javascript">
					document.location.href="modificar.php?mensaje=2";
				</script>
			<?php } else { ?>
				<script type="text/javascript">
					document.location.href="modificar.php?mensaje=3";
				</script>
			<?php }
		} else { ?>
			<script type="text/javascript">
				document.location.href="modificar.php?mensaje=1";
			</script>
		<?php
		}
	} else {
	?>
	<form method="post" action="modificar.php" id="form" name="form" accept-charset="utf-8">
	<input type="hidden" name="nombre" value="<?php echo $nombre; ?>" />
	<input type="hidden" name="apellidos" value="<?php echo $apellidos; ?>" />
	<input type="hidden" name="email" value="<?php echo $email; ?>" />
	<input type="hidden" name="pais" value="<?php echo $pais; ?>" />
	<input type="hidden" name="password" value="<?php echo $password; ?>" />
	<input type="hidden" name="password2" value="<?php echo $password2; ?>" />
	<input type="hidden" name="password3" value="<?php echo $password3; ?>" />
	<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
	</form>
	<script type="text/javascript">
		document.form.submit();
	</script>
	<?php
	}
	mysql_close($link);
	?>

</body>
</html>