<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>CURSOS (Imágenes)</h1>
		<h2><?php echo fecha(); ?></h2>
		<h3>Debes subir una imagen identificativa del curso. Si la imagen ya existe y quieres borrarla haz clic sobre la misma y sube una nueva.<br /><em>Te recuerdo que un curso, para su correcta visualización, debe llevar una imagen</em></h3>
		<form method="post" action="curso-imagen-modificar.php" accept-charset="utf-8" enctype="multipart/form-data">
			<?php
			$query="SELECT id,titulo,imagen,tipo_curso FROM cursos WHERE id=".$_GET["id_curso"];
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
			<h4>Curso: <strong><?php echo utf8_encode($row["titulo"]); ?></strong></h4>
			<div class="contacto-imagen">
				<?php
				if ($row["tipo_curso"] == "PRESENCIAL") {
					$carpeta = "presencial";
				} else if ($row["tipo_curso"] == "ONLINE") {
					$carpeta = "online";
				}
				if ($row["imagen"]) { ?>
				<p><a title="Eliminar imagen <?php echo $row["imagen"]; ?>" href="curso-imagen-eliminar.php?id_curso=<?php echo $_GET["id_curso"]; ?>&carpeta=<?php echo $carpeta; ?>"><img class="eliminar" src="../images/cursos/<?php echo $carpeta; ?>/<?php echo $row["imagen"]; ?>"></a>&nbsp;Nombre: <strong><?php echo $row["imagen"]; ?></strong></p>
				<?php } else { ?>
				<p>El artículo seleccionado no tiene imagen asociada</p>
				<?php } ?>
				<br />
				<p>Selecciona la nueva imagen para la sección. (Tamaño recomendado: 280px por 265px)</p>
			</div>
			<input type="hidden" name="id_curso" id="id_curso" value="<?php echo $row["id"]; ?>" />
			<input type="hidden" name="carpeta" id="carpeta" value="<?php echo $carpeta; ?>" />
			<input type="file" name="imagen" id="imagen" /><br />
			<button type="reset" id="cancelarcursos" name="cancelarcursos">VOLVER</button>
			<button type="button" id="cursoimagen" name="cursoimagen">AÑADIR o MODIFICAR IMAGEN</button>
			<?php
			}
			?>
		</form>
		<div class="mensajes">
			<?php if ($_GET["mensaje"] == "ok") {
				echo "<p>Has modificado la imagen de la sección</p>";
			} elseif ($_GET["mensaje"] == "error") {
				echo "<p>Se ha producido un error</p>";
			} else {
				echo "<p>Para una correcta visualización la imagen debe ser de 280px por 265px</p>";
			}
			?>
		</div>
	</div>
</div>
</body>

</html>
