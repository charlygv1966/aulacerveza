<?php
header('Content-Type: text/html; charset=UTF-8');
include("admin/inc/conexion.php");
include("inc/funciones.php");

if ($_POST) {
	$nombre=$_POST["nombre"];
	$email=$_POST["email"];
	$telefono=$_POST["telefono"];
	$asunto=$_POST["asunto"];
	$mensaje=$_POST["mensaje"];
	$msg=$_POST["msg"];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Contacto</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="contacto">
			<?php
			$query="SELECT * FROM contacto ORDER BY fecha_actualizacion";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			$contacto_email = $row["contacto_email"];
			?>
			<article>
				<header>
					<h2>Tlf: <?php echo $row["contacto_telefono"]; ?></h2>
					<h3><?php echo $row["contacto_email"]; ?></h3>
				</header>
				<figure>
					<img src="images/contacto/<?php echo $row["contacto_imagen"]; ?>" alt="Contacto" />
					<figcaption><?php echo $row["contacto_pietexto"]; ?></figcaption>
				</figure>
			</article>
			<?php
			}
			?>
			<div id="formulariocontacto">
				<form method="post" id="formu1ario" accept-charset="utf-8" action="contactar.php">
					<input type="hidden" name="contacto_email" id="contacto_email" value="<?php echo $contacto_email; ?>" />
					<p>
					<label for="nombre">Nombre: *</label>
					<input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre" value="<?php echo $nombre; ?>" />
					</p>
					<p>
					<label for="email">E-mail: *</label>
					<input type="text" name="email" id="email" placeholder="Introduce tu e-mail" value="<?php echo $email; ?>" />
					</p>
					<p>
					<label for="telefono">Teléfono:</label>
					<input type="text" name="telefono" id="telefono" placeholder="Introduce tu teléfono" value="<?php echo $telefono; ?>" />
					</p>
					<p>
					<label for="asunto">Asunto: *</label>
					<input type="text" name="asunto" id="asunto" placeholder="Introduce el asunto" value="<?php echo $asunto; ?>" />
					</p>
					<p>
					<label for="mensaje">Mensaje: *</label>
					<textarea name="mensaje" id="mensaje" placeholder="Introduce el mensaje"><?php echo $mensaje; ?></textarea>
					</p>
					<p class="requeridos">Campos requeridos *</p>
					<button type="submit" id="contactar" value="Contactar">Enviar</button>
					<div class="mensajescontacto">
						<?php include("inc/mensajes-contacto.php"); ?>
					</div>
				</form>
			</div>
			<?php
			mysql_close($link);
			?>
		</section>
	</section>
</div>

</body>
</html>

