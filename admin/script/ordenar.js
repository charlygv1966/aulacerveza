// JavaScript Document
// utilizo la libreria v1.11.1 de jQuery
// utilizo la libreria v1.11.1 de jQuery UI
$(document).ready(function() {
    $("#secciones-web").sortable({
        revert: true,
        axis: 'y',
        cursor: 'wait',
        opacity: 0.6,
        cursor: 'pointer',
        update: function() {
            var order = $('#secciones-web').sortable('serialize');
            $.post("ajax.php", order, function() {
                $('#success')
                    .html('Has reordenado las secciones de la web')
                    .fadeIn('slow')
                    .delay(2000)
                    .fadeOut('slow');
            });
        }
    });
});