<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>CURSOS</h1>
		<?php
		$cursotitulo = trim($_POST["cursotitulo"]);
		$cursodescripcion = $_POST["cursodescripcion"];
		$cursolugar = $_POST["cursolugar"];
		$cursoplazas = $_POST["cursoplazas"];
		$cursoforma_pago = $_POST["cursoforma_pago"];
		$cursotipo = $_POST["cursotipo"];
		$cursoprecio = normalizacion_precios($_POST["cursoprecio"]);
		$cursocomienzo = $_POST["cursocomienzo"];
		$cursofinal = $_POST["cursofinal"];
		$cursopublicado = trim($_POST["cursopublicado"]);
		$envio=1;

		if (esVacio($cursotitulo)) {
			$envio=0;
			$msg="Introduce el título del curso";
		} else if(esVacio($cursodescripcion)) {
			$envio=0;
			$msg="Introduce la descripcion del curso";
		} else if(esVacio($cursolugar)) {
			$envio=0;
			$msg="Introduce el lugar de realización del curso";
		} else if(esVacio($cursoplazas)) {
			$envio=0;
			$msg="Debes indicar el número de plazas del curso";
		} else if(esVacio($cursoforma_pago)) {
			$envio=0;
			$msg="Introduce la forma de pago del curso";
		} else if(esVacio($cursotipo)) {
			$envio=0;
			$msg="Selecciona el tipo del curso (PRESENCIAL u ONLINE)";
		} else if(esVacio($cursoprecio)) {
			$envio=0;
			$msg="Introduce el precio del curso";
		} else if(esVacio($cursocomienzo)) {
			$envio=0;
			$msg="Introduce la fecha de comienzo del curso";
		} else if(esVacio($cursofinal)) {
			$envio=0;
			$msg="Introduce la fecha de finalización del curso";
		} else if(fechacurso($cursofinal) < fechacurso($cursocomienzo)) {
			$envio=0;
			$msg="La fecha de finalización del curso debe ser igual o mayor que la fecha de comienzo";
		} else if(esVacio($cursopublicado)){
			$envio=0;
			$msg="Debes indicar si quieres publicar o no el curso";
		}

		if ($envio) {
			$query = "INSERT INTO cursos (titulo, descripcion, lugar_realizacion, plazas, forma_pago, tipo_curso, precio_curso, fecha_comienzo, fecha_final, publicado) VALUES ('".utf8_decode(normalizacion($cursotitulo))."','".utf8_decode($cursodescripcion)."','".utf8_decode($cursolugar)."',".$cursoplazas.",'".utf8_decode($cursoforma_pago)."','".utf8_decode($cursotipo)."',".$cursoprecio.",'".fechacurso($cursocomienzo)."','".fechacurso($cursofinal)."','".utf8_decode($cursopublicado)."')";

			$result=mysql_query($query);
			$id_curso = mysql_insert_id($link);
			mysql_close($link);
				if ($result){
				?>
				<script type="text/javascript">
					document.location.href="curso-imagen-actualizar.php?id_curso=<?php echo $id_curso; ?>&mensaje=2";
				</script>
				<?php
				} else {
				?>
				<script type="text/javascript">
					document.location.href="nuevo-curso.php?mensaje=3#mensajes";
				</script>
				<?php
				}
		} else {
		?>
		<form method="post" action="nuevo-curso.php#mensajes" id="form" name="form" accept-charset="utf-8">
		<input type="hidden" name="cursotitulo" value="<?php echo $cursotitulo; ?>" />
		<input type="hidden" name="cursodescripcion" value="<?php echo $cursodescripcion; ?>" />
		<input type="hidden" name="cursolugar" value="<?php echo $cursolugar; ?>" />
		<input type="hidden" name="cursoplazas" value="<?php echo $cursoplazas; ?>" />
		<input type="hidden" name="cursoforma_pago" value="<?php echo $cursoforma_pago; ?>" />
		<input type="hidden" name="cursotipo" value="<?php echo $cursotipo; ?>" />
		<input type="hidden" name="cursoprecio" value="<?php echo $cursoprecio; ?>" />
		<input type="hidden" name="cursocomienzo" value="<?php echo $cursocomienzo; ?>" />
		<input type="hidden" name="cursofinal" value="<?php echo $cursofinal; ?>" />
		<input type="hidden" name="cursopublicado" value="<?php echo $cursopublicado; ?>" />
		<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
		</form>
		<script type="text/javascript">
			document.form.submit();
		</script>
		<?php
		}
		?>
	</div>
</div>
</body>

</html>
