<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>RECURSOS (Textos)</h1>
		<?php
		$recursotitulo = trim($_POST["recursotitulo"]);
		$recursotextos = trim($_POST["recursotextos"]);
		$recursopublicado = trim($_POST["recursopublicado"]);
		$envio=1;

		if (esVacio($recursotitulo)) {
			$envio=0;
			$msg="Introduce el título del recurso";
		} else if(esVacio($recursotextos)){
			$envio=0;
			$msg="Introduce el contenido del recurso";
		} else if(esVacio($recursopublicado)){
			$envio=0;
			$msg="Debes indicar si quieres publicar o no el recurso";
		}

		if ($envio) {
			$query = "INSERT INTO recursos (titulo, textos, publicado, fecha_actualizacion) VALUES ('". utf8_decode(normalizacion($recursotitulo))."','".utf8_decode($recursotextos)."','".utf8_decode(normalizacion($recursopublicado))."', now())";
			$result=mysql_query($query);
			$id_recurso = mysql_insert_id($link);
			mysql_close($link);
				if ($result){
				?>
				<script type="text/javascript">
					document.location.href="recurso-imagen-actualizar.php?id_recurso=<?php echo $id_recurso; ?>&amp;mensaje=2";
				</script>
				<?php
				} else {
				?>
				<script type="text/javascript">
					document.location.href="nuevo-recurso.php?mensaje=3";
				</script>
				<?php
				}
		} else {
		?>
		<form method="post" action="nuevo-recurso.php" id="form" name="form" accept-charset="utf-8">
		<input type="hidden" name="recursotitulo" value="<?php echo $recursotitulo; ?>" />
		<input type="hidden" name="recursotextos" value="<?php echo $recursotextos; ?>" />
		<input type="hidden" name="recursopublicado" value="<?php echo $recursopublicado; ?>" />
		<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
		</form>
		<script type="text/javascript">
			document.form.submit();
		</script>
		<?php
		}
		?>
	</div>
</div>
</body>

</html>
