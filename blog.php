<?php
header('Content-Type: text/html; charset=UTF-8');
include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Blog</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-ui-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="blog">
			<section id="bloger">
				<?php
				//primero veo cuantos registros hay que mostrar
				$query_listado = "SELECT blogs FROM configuracion_general";
				$result_listado=mysql_query($query_listado);
				while ($row_listado=mysql_fetch_array($result_listado)) {
					$total_listado_blogs = $row_listado["blogs"];
				}
				//consulta para la paginación
				$query0 = "SELECT * FROM blog WHERE publicado='si'";
				$result0=mysql_query($query0);
				$num_total_registros = mysql_num_rows($result0);
				//defino cantidad de artículos por página
				$TAMANO_PAGINA = $total_listado_blogs;
				//calculo el total de páginas
				$total_paginas = ceil($num_total_registros / $TAMANO_PAGINA);
				//examino la página a mostrar y el inicio del registro a mostrar
				$pagina = $_GET["pagina"];
				if (!$pagina || $pagina > $total_paginas) {
					$inicio = 0;
					$pagina=1;
				} else {
					$inicio = ($pagina - 1) * $TAMANO_PAGINA;
				}

				$query="SELECT * FROM blog WHERE publicado='si' ORDER BY fecha_publicacion DESC LIMIT " . $inicio . "," . $TAMANO_PAGINA;
				$result=mysql_query($query);
				if ($num_total_registros > 0) {
					while ($row=mysql_fetch_array($result)) {
					?>
						<article>
							<figure>
								<img src="images/blog/secciones/<?php echo $row["imagen"]; ?>" alt="<?php echo utf8_encode($row["titulo"]); ?>" />
								<figcaption><?php echo utf8_encode($row["titulo"]); ?></figcaption>
							</figure>
							<header>
								<h2><?php echo utf8_encode($row["titulo"]); ?></h2>
							</header>
							<div class="textos">
								<?php echo cortaStrings($row["textos"],200); ?>
								<div class="vermas"><a href="blog_descripcion.php?id_blog=<?php echo $row["id"]; ?>&pagina=<?php echo $pagina; ?>">Ver más &raquo;</a></div>
							</div>
							<footer><?php echo "Publicado: ".fecha($row["fecha_publicacion"]); ?></footer>
						</article>
					<?php
					}
				} else {
				echo "<p>NO SE HAN ENCONTRADO REGISTROS</p>";
				}
				?>

				<?php
				if ($total_paginas > 1){
				?>
				<div class="navegacion">
					<ul>
						<?php
						// PÁGINA ANTERIOR
						if ($pagina > 1) {
							echo "<li class=\"anterior\"><a href='".$_SERVER['PHP_SELF']."?pagina=" . ($pagina - 1) ."' title=\"Página anterior\">anterior</a></li>";
						} else {
							echo "<li class=\"desactivado\">anterior</li>";
						}

						// PÁGINA SIGUIENTE
						if ($pagina < $total_paginas) {
							echo "<li class=\"siguiente\"><a title=\"Página siguiente\" href='".$_SERVER['PHP_SELF']."?pagina=" . ($pagina + 1) . "'>siguiente</a></li>";
						} else {
							echo "<li class=\"desactivado\">siguiente</li>";
						}
						?>
					</ul>
				</div>
				<?php
				}
				?>
			</section>

			<aside>
				<div id="ultimos">
					<ul id="actuales">
						<li><a href="#">Últimas 10 entradas</a>
							<ul>
							<?php
							$query="SELECT * FROM blog WHERE publicado='si' ORDER BY fecha_publicacion DESC LIMIT 10";
							$result=mysql_query($query);
							while ($row=mysql_fetch_array($result)) {
							?>
								<li><a href="blog_descripcion.php?id_blog=<?php echo $row["id"]; ?>"><?php echo utf8_encode($row["titulo"]); ?></a></li>
							<?php
							}
							?>
							</ul>
						</li>
					</ul>
				</div>

				<div id="historico">
					<h3>Más actuales</h3>
					<ul id="historial">
						<?php
						$anio_actual = date("Y");
						$mes_actual = date("m");
						$query="SELECT DISTINCT YEAR(fecha_publicacion) AS anio FROM blog WHERE publicado='si' ORDER BY fecha_publicacion DESC";
						$result=mysql_query($query);
						while ($row=mysql_fetch_array($result)) {
						?>
						<li class="anio<?php if ($anio_actual == $row["anio"]) { echo " sel"; } ?>"><a href="#"><?php echo $row["anio"];?></a>
							<?php
							$query1="SELECT DISTINCT MONTH(fecha_publicacion) AS mes FROM blog WHERE publicado='si' AND YEAR(fecha_publicacion)=".$row["anio"]." ORDER BY fecha_publicacion DESC";
							$result1=mysql_query($query1);
							if ($row1=mysql_fetch_array($result1)) {
							?>
								<ul class="meses<?php if (($anio_actual == $row["anio"]) && ($mes_actual == $row1["mes"])) { echo " mostrar"; } ?>">
									<?php
									$query2="SELECT DISTINCT MONTH(fecha_publicacion) AS mes FROM blog WHERE publicado='si' AND YEAR(fecha_publicacion)=".$row["anio"]." ORDER BY fecha_publicacion DESC";
									$result2=mysql_query($query2);
									while ($row2=mysql_fetch_array($result2)) {
									?>
									<li class="mes<?php if (($anio_actual == $row["anio"]) && ($mes_actual == $row2["mes"])) { echo " sel"; } ?>"><a href="#"><?php echo pintarMes($row2["mes"]); ?></a>
										<ul class="unmes<?php if (($anio_actual == $row["anio"]) && ($mes_actual == $row2["mes"])) { echo " mostrar"; } ?>">
										<?php
											$anio_p = $row["anio"];
											$mes_p = $row2["mes"];
											$query3="SELECT id, titulo FROM blog WHERE publicado='si' AND YEAR(fecha_publicacion)='". $anio_p ."' AND MONTH(fecha_publicacion)='".$mes_p."' ORDER BY fecha_publicacion DESC";
											$result3=mysql_query($query3);
											while ($row3=mysql_fetch_array($result3)) {
											?>
												<li><a href="blog_descripcion.php?id_blog=<?php echo $row3["id"]; ?>"><?php echo utf8_encode($row3["titulo"]); ?></a></li>
										<?php
										}
										?>
										</ul>
									</li>
									<?php
									}
									?>
								</ul>
							<?php
							}
							?>
						</li>
						<?php
						}
						mysql_close($link);
						?>
					</ul>
				</div>
			</aside>
			<a href="#" class="scrolltop"></a>
		</section>
	</section>
</div>

</body>
</html>


