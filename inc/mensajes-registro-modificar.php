<?php
	$mensajeenviado = $_GET["mensaje"];
	switch ($mensajeenviado) {
		case '1':
			$mensaje = "<strong>La contraseña actual indicada no es correcta</strong><br />Para modificar tus datos debes incluirla";
			break;
		case '2':
			$mensaje = "<strong>Has modificado tus datos de conexión</strong>";
			break;
		case '3':
			$mensaje = "<strong>Se ha producido un error</strong><br />&iexcl;Inténtalo más tarde!";
			break;
		default:
			$mensaje = "";
			break;
	}
	echo $mensaje;

	if ($msg=="" && (!isset($_GET["mensaje"]))) {
		$msg = "Debes incluir correctamente todos los campos";
	}
	echo $msg;

	if ($mensajeenviado == 2) { ?>
		<script type="text/javascript">
			alert("La página se cerrará.\n\nDeberás logarte de nuevo con tus nuevos datos");
			document.location.href="inc/cerrar.php";
		</script>
	<?php
	}
?>
