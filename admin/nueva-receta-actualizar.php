<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>RECETAS (Textos)</h1>
		<?php
		$recetatitulo = trim($_POST["recetatitulo"]);
		$recetatextos = trim($_POST["recetatextos"]);
		$recetapublicado = trim($_POST["recetapublicado"]);
		$envio=1;

		if (esVacio($recetatitulo)) {
			$envio=0;
			$msg="Introduce el título de la receta";
		} else if(esVacio($recetatextos)){
			$envio=0;
			$msg="Introduce el contenido de la receta";
		} else if(esVacio($recetapublicado)){
			$envio=0;
			$msg="Debes indicar si quieres publicar o no la receta";
		}

		if ($envio) {
			$query = "INSERT INTO recetas (titulo, textos, publicado, fecha_actualizacion) VALUES ('". utf8_decode(normalizacion($recetatitulo))."','".utf8_decode($recetatextos)."','".utf8_decode(normalizacion($recetapublicado))."', now())";
			$result=mysql_query($query);
			$id_receta = mysql_insert_id($link);
			mysql_close($link);
				if ($result){
				?>
				<script type="text/javascript">
					document.location.href="receta-imagen-actualizar.php?id_receta=<?php echo $id_receta; ?>&amp;mensaje=2";
				</script>
				<?php
				} else {
				?>
				<script type="text/javascript">
					document.location.href="nueva-receta.php?mensaje=3";
				</script>
				<?php
				}
		} else {
		?>
		<form method="post" action="nueva-receta.php" id="form" name="form" accept-charset="utf-8">
		<input type="hidden" name="recetatitulo" value="<?php echo $recetatitulo; ?>" />
		<input type="hidden" name="recetatextos" value="<?php echo $recetatextos; ?>" />
		<input type="hidden" name="recetapublicado" value="<?php echo $recetapublicado; ?>" />
		<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
		</form>
		<script type="text/javascript">
			document.form.submit();
		</script>
		<?php
		}
		?>
	</div>
</div>
</body>

</html>
