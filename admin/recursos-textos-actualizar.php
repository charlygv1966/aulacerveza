<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>RECURSOS (Modificar contenidos)</h1>
		<?php
		if ($_POST) {
			$id_recurso = $_POST["id_recurso"];
			$titulo = trim(utf8_decode(normalizacion_textos($_POST["titulo"])));
			$recursotextos = trim($_POST["recursotextos"]);
			$publicado = trim($_POST["publicado"]);
			$envio=1;
		}

		if (esVacio($titulo)) {
			$envio=0;
			$msg="Introduce el título del artículo";
		} else if(esVacio($recursotextos)){
			$envio=0;
			$msg="Introduce el contenido del artículo";
		}

		if ($envio) {
			$query = "UPDATE recursos SET titulo='$titulo',textos='$recursotextos',publicado='$publicado',fecha_actualizacion=now() WHERE id=".$id_recurso;
			$result=mysql_query($query);
			mysql_close($link);
				if ($result){
				?>
				<script type="text/javascript">
					document.location.href="recursos-textos-modificar.php?mensaje=2&recurso=<?php echo $id_recurso; ?>";
				</script>
				<?php
				} else {
				?>
				<script type="text/javascript">
					document.location.href="recursos-textos-modificar.php?mensaje=3&recurso=<?php echo $id_recurso; ?>";
				</script>
				<?php
				}
		} else {
		?>
		<form method="post" action="recursos-textos-modificar.php?recurso=<?php echo $id_recurso; ?>" id="form" name="form" accept-charset="utf-8">
			<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
		</form>
		<script type="text/javascript">
			document.form.submit();
		</script>
		<?php
		}
		?>
	</div>
</div>
</body>

</html>
