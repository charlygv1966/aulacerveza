<?php
header('Content-Type: text/html; charset=UTF-8');
include("admin/inc/conexion.php");
include("inc/funciones.php");

if ($_POST) {
	$email=$_POST["email"];
	$msg=$_POST["msg"];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Contacto</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="registrogeneral">
			<h2><img src="images/registro/registro.png" alt="Registro"></h2>
			<h3>¿Has olvidado tu contraseña?</h3>
			<h4>Indícanos tu email y te la haremos llegar a tu correo</h4>
			<form method="post" id="formu1ario" action="enviar.php" accept-charset="utf-8">
				<p>
				<label for="email">EMail: *</label>
				<input type="text" name="email" id="email" placeholder="Introduce tu email" value="<?php echo $email; ?>" />
				</p>
				<p class="requeridos">Campos requeridos*</p>
				<button type="submit" id="recordar" value="Recordar">Enviar</button>
				<div class="mensajesrecordar">
					<?php include("inc/mensajes-recordar.php"); ?>
				</div>
			</form>
		</section>
	</section>
</div>

</body>
</html>

