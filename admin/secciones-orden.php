<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="script/ordenar.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>SECCIONES PRINCIPALES DE LA WEB</h1>
		<h2><?php echo fecha(); ?></h2>
		<h3>SECCIONES</h3>
		<h3>Desde esta pantalla puedes <strong>reordenar</strong> las secciones principales de la web.</h3>
		<h3>Para reordenar, arrastra la sección hasta su nueva posición.</h3>
		<ol id="secciones-web">
		<?php
			$query="SELECT * FROM menu WHERE espadre=1 ORDER BY position";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) { ?>
				<li id="<?php echo 'seccion_'.$row['id'] ?>" class="ui-state-default"><?php echo utf8_encode($row["nombre"]); ?></li>
			<?php
			}
			?>
		</ol>
		<button type="reset" id="cancelar" name="cancelar">VOLVER</button>

		<div class="mensajes">
			<div id="success"></div>
		</div>

	</div>
</div>
</body>

</html>
