<?php
session_start();
header('Content-Type: text/html; charset=UTF-8');
include("inc/funciones.php");
if (!$_SESSION["entrar"] == "SI") { ?>
	<!--header("Location:formulas.php?acceso=0");-->
	<form method="post" action="formulas.php" id="form" name="form" accept-charset="utf-8">
		<input type="hidden" name="msg" value="0" />
	</form>
	<script type="text/javascript">
		document.form.submit();
	</script>
<?php
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Fórmulas</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="formulas">
			<h2>Aquí irán las fórmulas cuyo usuario sea el propietario; no accesibles para otros usuarios validados, así que cada vez que se grabe una fórmula llevará asociado el <strong>id_usuario</strong></h2>
		</section>
	</section>
</div>

</body>
</html>

