$(document).ready(function() {
    //funciones generales de javascript
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scrolltop,.scrolltop2').fadeIn();
        } else {
            $('.scrolltop,.scrolltop2').fadeOut();
        }
    });

    $('.scrolltop,.scrolltop2').click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    //ocultamos de momento los idiomas con visibility para que se mantenga el espacio reservado
    $("#idiomas").css({ "visibility": "hidden" });
    //los enlaces externos que se abran en pantalla nueva
    $("a[href^='http://'], a[href^='https://']").attr("target", "_blank");

    //botones de registro
    $("button[name='registrarse']").bind("click", function() {
        document.location.href = "registro.php";
    });
    $("button[name='ok']").bind("click", function() {
        enviar();
    });
    //fin de botones de registro

    $("button#cancelar-reserva-presenciales").bind("click", function() {
        document.location.href = "cursos-presenciales.php";
    });
    $("button#cancelar-reserva-online").bind("click", function() {
        document.location.href = "cursos-online.php";
    });

    //boton de cancelar la modificación del registro
    $("button#cancelarmodi").bind("click", function() {
        document.location.href = "index.php";
    });

    //boton de imprimir
    $("#imprimir").bind("click", function() {
        window.print();
    });

    $("#compartir").bind("click", function() {
        $("#redes_compartir").fadeIn('slow');
    });

    $("#redes_compartir .cerrar a").bind("click", function() {
        $("#redes_compartir").fadeOut('slow');
    });

    $("a#condiciones-devolucion").bind("click", function() {
        condiciones = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sit amet justo id neque ultricies fringilla in nec est. Donec neque felis, sollicitudin tincidunrutrum sed libero eget, lobortis vestiba pur. Nullam non neque lacinia, rhoncus lectus eu, pretium justo.</p><p>Vivamus sit amet justo id neque ultricies fringilla in nec est. Donec neque felis, sollicitudin tincidunrutrum sed libero eget, lobortis vestiba pur. Nullam non neque lacinia, rhoncus lectus eu, pretium justo.</p><p>Donec neque felis, sollicitudin tincidunrutrum sed libero eget, lobortis vestiba pur. Nullam non neque lacinia, rhoncus lectus eu, pretium justo.</p>";
        $('<div></div>')
            .attr({
                id: 'condiciones-devolucion',
                display: 'none'
            })
            .html(condiciones)
            .appendTo('body')
            .dialog({
                resizable: 'false',
                width: '350',
                height: '350',
                modal: 'true',
                title: 'CONDICIONES DE DEVOLUCIÓN'
            }).css({ "font-family": "Arial, Helvetica, sans-serif" });
    });

    /* estilos para las listas de los blogs */
    $("ul#actuales > li > a").bind("click", function(event) {
        $("ul#actuales").toggleClass("sel");
        $("ul#actuales > li > ul").toggleClass("ocultar");
        event.preventDefault();
    });
    $("ul#historial > li.anio > a").bind("click", function(event) {
        $(this).closest("ul#historial > li.anio").toggleClass("sel");
        $(this).next("ul.meses").toggleClass("mostrar");
        event.preventDefault();
    });
    $("ul#historial > li.anio > ul.meses > li.mes > a").bind("click", function(event) {
        $(this).closest("ul#historial > li.anio > ul.meses > li.mes").toggleClass("sel");
        $(this).next("ul.meses").toggleClass("mostrar");
        event.preventDefault();
    });
    $("ul#historial > li.anio > ul.meses > li.mes > a").bind("click", function(event) {
        $(this).closest("ul#historial > li.anio > ul.meses > li.mes").toggleClass("seles");
        $(this).next("ul.unmes").toggleClass("mostrar");
        event.preventDefault();
    });
    /* fin de estilos para las listas de los blogs */

    //a partir de aquí van las diferentes funciones llamadas segun los diversos eventos

    //seccion para definir las diferentes funciones de jQuery
    function validar() {
        if ($("#usuario").val() == "") {
            alert("Por favor, introduce tu email");
            $("#usuario").focus();
            return false;
        }

        if (!esEmail($("#usuario").val())) {
            alert("Por favor, introduce un email correcto");
            $("#usuario").focus();
            return false;
        }

        if ($("#pass").val() == "") {
            alert("Por favor, introduce tu contraseña");
            $("#pass").focus();
            return false;
        }
        return true;
    }

    function enviar() {
        if (validar()) {
            $('#formregistro').submit();
        }
    }

    function esEmail(val) {
        arroba = val.lastIndexOf('@');
        if (arroba < 1) {
            return false;
        } else {
            punto = val.indexOf('.', arroba);
            if (punto < arroba + 2 || punto > val.length - 2)
                return false;
        }
        return true;
    }

    //el texto para la introducción de los foros
    CKEDITOR.replace('comentariosforo', {
        uiColor: '#b2bd34',
        resize_enabled: false,
        height: '150px',
        toolbar: [
            ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList']
        ]
    });

});