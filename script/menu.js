$(document).ready(function() {
    //añadimos una clase separadora entre las opciones del menu y borramos la última
    $("ul#topnav > li").after('<li class="separadormenu"></li>');
    $("ul#topnav > li:last-child").remove();
    //si hay un menu de segundo nivel con clase[seleccionado], también seleccionamos a su padre con clase[sel]
    $("ul#topnav > li > ul >li.seleccionado").each(function() {
        $(this).closest("ul#topnav > li").addClass('sel');
    });
    //estas son las clases en los separadores que definimos antes y despues de un menu principal seleccionado
    //ej: "ul#topnav li.quienes-somos.sel"
    var gruposel = $("li.separadormenu");
    //este código es para que ie8 no haga un efecto raro que hace con el primer li del menu
    $("ul#topnav li:first-child").mouseover(function() {});
    $("ul#topnav > li.sel").each(function() {
        var value = $(this).attr("class");
        $(this).prev(gruposel).removeClass('separadormenu').addClass("separadormenu-" + value).removeClass("sel");
        $(this).next(gruposel).removeClass('separadormenu').addClass("separadormenu-" + value).removeClass("sel");
        /* añadido especial para las páginas secundarias de las secciones */
        //ej: recursos.php -> recursos-recurso.php
        //en todas las secciones no hay problema pero en cursos - recursos hay que comprobar la sección actual
        //ya que la palabra cursos está dentro de recursos
        if ($(this).hasClass("recursos")) {
            $("ul#topnav > li.cursos").removeClass("sel");
            $("ul#topnav > li.cursos").prev("li.separadormenu-cursos").removeClass("sel").removeClass("separadormenu-cursos").addClass("separadormenu");
            $("ul#topnav > li.cursos").next("li.separadormenu-cursos").removeClass("sel").removeClass("separadormenu-cursos").addClass("separadormenu");
        }
        /* fin de añadido especial*/
        /* para las páginas de cursos */
        var paginaActual = document.location.pathname;
        if (buscaPagina(paginaActual, "cursos-presenciales")) {
            $("ul#topnav > li.cursos > ul > li:first-child").addClass("seleccionado");
        }
        if (buscaPagina(paginaActual, "cursos-online")) {
            $("ul#topnav > li.cursos > ul > li:last-child").addClass("seleccionado");
        }
        /* fin para las páginas de cursos */
    });
});

/* función que detecta si las páginas de cursos son presenciales u online para que las pantallas secundarias activen el menu */
function buscaPagina(paginactual, cadenabusqueda) {
    existecadena = paginactual.indexOf(cadenabusqueda);
    if (existecadena == -1) {
        return null;
    }
    return paginactual.substring(0, existecadena);
}