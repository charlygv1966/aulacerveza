<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");

	if ($_POST) {
		$recursotitulo = $_POST["recursotitulo"];
		$recursotextos = $_POST["recursotextos"];
		$recursopublicado = $_POST["recursopublicado"];
		$msg=$_POST["msg"];
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>RECURSOS (Nuevo recurso)</h1>
		<h2><?php echo fecha(); ?></h2>
		<form method="post" accept-charset="utf-8" action="nuevo-recurso-actualizar.php">
			<p>
				<label for="recursotitulo">Título</label>
				<input type="text" name="recursotitulo" id="recursotitulo" placeholder="Introduce el título del recurso" value="<?php echo $recursotitulo; ?>" />
			</p>
			<p>
				<label for="recursotextos">Texto del artículo</label>
				<textarea id="recursotextos" name="recursotextos"><?php echo $recursotextos; ?></textarea>
			</p>
			<br />
			<p>
				<label for="recursopublicado">Publicado</label>
				<select name="recursopublicado" id="recursopublicado">
					<option value="si" <?php if ($recursopublicado == "si") { echo "selected=\"selected\""; } ?>>Si</option>
					<option value="no" <?php if ($recursopublicado == "no") { echo "selected=\"selected\""; } ?>>No</option>
				</select>
			</p>
			<p>
				<button type="reset" id="cancelar" name="cancelar">VOLVER</button>
				<button type="submit" id="recursos" name="recursos">ACEPTAR</button>
			</p>
			<div class="separator"></div>
			<p>
				Si el artículo que añadas es correcto y se graba en la base de datos, te redireccionaremos a la pantalla en la que deberás añadir (opcional) una imagen.
			</p>
		</form>

		<div class="mensajes">
		<?php
			$mensajeenviado = $_GET["mensaje"];
			switch ($mensajeenviado) {
				case '2':
					$mensaje = "Has actualizado el contenido";
					break;
				case '3':
					$mensaje = "Se ha producido un error";
					break;
				default:
					$mensaje = "";
					break;
			}
			echo $mensaje;
			if ($msg=="" && (!isset($_GET["mensaje"]))) {
				$msg = "Debes incluir correctamente todos los campos";
			}
			echo $msg;
		?>
		</div>

	</div>
</div>
</body>

</html>
