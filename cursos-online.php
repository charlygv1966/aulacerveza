<?php
header('Content-Type: text/html; charset=UTF-8');
include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Cursos online</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="cursos">
			<div id="listado">
				<h2><img src="images/cursos/online.png" alt="ONLINE"></h2>
				<?php
				//primero veo cuantos registros hay que mostrar
				$query_listado = "SELECT cursos FROM configuracion_general";
				$result_listado=mysql_query($query_listado);
				while ($row_listado=mysql_fetch_array($result_listado)) {
					$total_listado_cursos = $row_listado["cursos"];
				}
				//consulta para la paginación
				$query0 = "SELECT * FROM cursos WHERE tipo_curso='ONLINE' AND publicado='si'";
				$result0=mysql_query($query0);
				$num_total_registros = mysql_num_rows($result0);
				//defino cantidad de artículos por página
				$TAMANO_PAGINA = $total_listado_cursos;
				//calculo el total de páginas
				$total_paginas = ceil($num_total_registros / $TAMANO_PAGINA);
				//examino la página a mostrar y el inicio del registro a mostrar
				$pagina = $_GET["pagina"];
				if (!$pagina || $pagina > $total_paginas) {
					$inicio = 0;
					$pagina=1;
				} else {
					$inicio = ($pagina - 1) * $TAMANO_PAGINA;
				}

				$query="SELECT * FROM cursos WHERE tipo_curso='ONLINE' AND publicado='si' ORDER BY fecha_comienzo LIMIT " . $inicio . "," . $TAMANO_PAGINA;
				$result=mysql_query($query);
				if ($num_total_registros > 0) {
					while ($row=mysql_fetch_array($result)) {
					?>
					<article>
						<figure>
							<?php if ($row["imagen"]) { ?>
								<img src="images/cursos/online/<?php echo $row["imagen"]; ?>" alt="<?php echo utf8_encode($row["titulo"]); ?>">
							<?php } else { ?>
								<img src="images/cursos/nofoto.png" alt="">
							<?php } ?>
						</figure>
						<div class="descripcion">
							<h3><a href="cursos-online-descripcion.php?curso=<?php echo $row["id"]; ?>&pagina=<?php echo $pagina; ?>"><?php echo utf8_encode($row["titulo"]); ?></a></h3>
							<h4><?php echo fecha_foro($row["fecha_comienzo"]); ?> - <?php echo fecha_foro($row["fecha_final"]); ?></h4>
							<?php echo cortaStrings($row["descripcion"],150); ?>
						</div>
					</article>
					<?php
					}
				} else {
				echo "<p>NO SE HAN ENCONTRADO REGISTROS</p>";
				}
				mysql_close($link);
				?>
			</div>

			<?php
			if ($total_paginas > 1){
			?>
			<div class="navegacion">
				<ul>
					<?php
					// PÁGINA ANTERIOR
					if ($pagina > 1) {
						echo "<li class=\"anterior\"><a href='".$_SERVER['PHP_SELF']."?pagina=" . ($pagina - 1) ."' title=\"Página anterior\">anterior</a></li>";
					} else {
						echo "<li class=\"desactivado\">anterior</li>";
					}

					// PÁGINA SIGUIENTE
					if ($pagina < $total_paginas) {
						echo "<li class=\"siguiente\"><a title=\"Página siguiente\" href='".$_SERVER['PHP_SELF']."?pagina=" . ($pagina + 1) . "'>siguiente</a></li>";
					} else {
						echo "<li class=\"desactivado\">siguiente</li>";
					}
					?>
				</ul>
			</div>
			<?php
			}
			?>
		</section>
	</section>
</div>

</body>
</html>
