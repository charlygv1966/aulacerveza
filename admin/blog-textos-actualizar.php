<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>BLOG (Modificar contenidos)</h1>
		<?php
		if ($_POST) {
			$id_blog = $_POST["id_blog"];
			$titulo = trim(utf8_decode(normalizacion_textos($_POST["titulo"])));
			$blogtextos = trim($_POST["blogtextos"]);
			$publicado = trim($_POST["publicado"]);
			$envio=1;
		}

		if (esVacio($titulo)) {
			$envio=0;
			$msg="Introduce el título del artículo";
		} else if(esVacio($blogtextos)){
			$envio=0;
			$msg="Introduce el contenido del artículo";
		}

		if ($envio) {
			$query = "UPDATE blog SET titulo='$titulo',textos='$blogtextos',publicado='$publicado' WHERE id=".$id_blog;
			$result=mysql_query($query);
			mysql_close($link);
				if ($result){
				?>
				<script type="text/javascript">
					document.location.href="blog-textos-modificar.php?mensaje=2&blog=<?php echo $id_blog; ?>";
				</script>
				<?php
				} else {
				?>
				<script type="text/javascript">
					document.location.href="blog-textos-modificar.php?mensaje=3&blog=<?php echo $id_blog; ?>";
				</script>
				<?php
				}
		} else {
		?>
		<form method="post" action="blog-textos-modificar.php?blog=<?php echo $id_blog; ?>" id="form" name="form" accept-charset="utf-8">
			<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
		</form>
		<script type="text/javascript">
			document.form.submit();
		</script>
		<?php
		}
		?>
	</div>
</div>
</body>

</html>
