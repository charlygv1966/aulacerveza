<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");

	if ($_POST) {
		$emailcontacto = $_POST["emailcontacto"];
		$telefonocontacto = $_POST["telefonocontacto"];
		$textocontacto = $_POST["textocontacto"];
		$msg=$_POST["msg"];
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>CONTACTO (textos)</h1>
		<h2><?php echo fecha(); ?></h2>
		<form method="post" action="contacto-textos-modificar.php" accept-charset="utf-8">
			<?php
			$query="SELECT * FROM contacto ORDER BY fecha_actualizacion";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
				<p>
					<label for="emailcontacto">Email de contacto</label>
					<input type="text" name="emailcontacto" id="emailcontacto" placeholder="Introduce el email de contacto" value="<?php if ($row) { echo $row["contacto_email"]; } else { echo $emailcontacto; } ?>" />
				</p>
				<p>
					<label for="telefonocontacto">Teléfono de contacto</label>
					<input type="text" name="telefonocontacto" id="telefonocontacto" placeholder="Introduce el teléfono de contacto" value="<?php  if ($row) { echo $row["contacto_telefono"]; } else { echo $telefonocontacto; } ?>" />
				</p>
				<p>
					<label for="textocontacto">Texto del pie de contacto</label>
					<input type="text" name="textocontacto" id="textocontacto" placeholder="Introduce el texto descriptivo del pie de la imagen" value="<?php  if ($row) { echo $row["contacto_pietexto"]; } else { echo $textocontacto; } ?>" />
				</p>
				<button type="reset" id="cancelar" name="cancelar">VOLVER</button>
				<button type="submit" id="contacto" name="contacto">MODIFICAR</button>
			<?php
			}
			?>
		</form>
		<div class="mensajes">
		<?php
			$mensajeenviado = $_GET["mensaje"];
			switch ($mensajeenviado) {
				case '2':
					$mensaje = "Has actualizado el contenido";
					break;
				case '3':
					$mensaje = "Se ha producido un error";
					break;
				default:
					$mensaje = "";
					break;
			}
			echo $mensaje;
			if ($msg=="" && (!isset($_GET["mensaje"]))) {
				$msg = "Debes incluir correctamente todos los campos";
			}
			echo $msg;
		?>
		</div>
	</div>
</div>
</body>

</html>
