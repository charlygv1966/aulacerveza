<?php
header('Content-Type: text/html; charset=UTF-8');
include("inc/funciones.php");
$foro = $_GET["foro"];
$pageforo = $_GET["pageforo"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Foros</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="foros">
			<section id="foro">
				<header>
					<div id="mensaje">FORO</div>
					<div id="fecha">PUBLICADO</div>
				</header>
				<?php
				$query="SELECT * FROM foros WHERE id='".$foro."' AND publicado='si'";
				$result=mysql_query($query);
				if ($row=mysql_fetch_array($result)) {
				?>
				<div id="titularforo">
					<div id="titulo">
						<h2>Asunto: <?php echo utf8_encode($row["titulo"]); ?></h2>
					</div>
					<div id="fechaforo">
						<h2><?php echo fecha_foro($row["fecha_publicacion"]); ?></h2>
					</div>
				</div>

				<div id="titulares">
					<div id="nombreusuario">Usuario</div>
					<div id="mensajeusuario">Mensaje</div>
				</div>
				<?php
					//primero veo cuantos registros hay que mostrar
					$query_listado = "SELECT foros_comentarios FROM configuracion_general";
					$result_listado=mysql_query($query_listado);
					while ($row_listado=mysql_fetch_array($result_listado)) {
						$total_listado_foros_comentarios = $row_listado["foros_comentarios"];
					}
					//consulta para la paginación
					//$query="SELECT * FROM foros WHERE id='".$foro."' AND publicado='si'";
					$query0 = "SELECT * FROM foros_comentarios WHERE id_foro='".$foro."' AND publicado='si' ORDER BY fecha_publicacion";
					$result0=mysql_query($query0);
					$num_total_registros = mysql_num_rows($result0);
					//defino cantidad de artículos por página
					$TAMANO_PAGINA = $total_listado_foros_comentarios;
					//calculo el total de páginas
					$total_paginas = ceil($num_total_registros / $TAMANO_PAGINA);

					//examino la página a mostrar y el inicio del registro a mostrar
					$pagina = $_GET["pagina"];
					// cargo la última página de los comentarios de cada foro
					if (!$pagina) {
						$pagina = $total_paginas;
					}

					if (!$pagina || $pagina > $total_paginas) {
						$inicio = 0;
						$pagina = 1;
					} else {
						$inicio = ($pagina - 1) * $TAMANO_PAGINA;
					}

					//si no he definido el rango de la navegación muestro 5 páginas
					if ($range == "" || $range == 0) $range = 5;
					//defino el rango izquierda - derecha de la página actual
					$lrange = max(1,$pagina-(($range-1)/2));
					$rrange = min($total_paginas,$pagina+(($range-1)/2));

					if (($rrange - $lrange) < ($range - 1)) {
						if ($lrange == 1) {
							$rrange = min($lrange + ($range-1), $total_paginas);
						} else {
							$lrange = max($rrange - ($range-1), 0);
						}
					}
				}
				// reducir la linea de consulta utilizando alias
				//$query="SELECT foros.id, foros.titulo AS titulo, foros.publicado, foros.fecha_publicacion AS fecha_publicacion_foro, foros_comentarios.id, foros_comentarios.id_foro, foros_comentarios.id_usuario, foros_comentarios.comentario AS foros_comentarios_comentario, foros_comentarios.publicado, foros_comentarios.fecha_publicacion AS fecha_publicacion_comentario, usuarios.id AS identificacion_usuario, usuarios.nombre AS nombre_usuario, usuarios.apellidos AS apellidos_usuario, usuarios.email FROM foros INNER JOIN foros_comentarios INNER JOIN usuarios ON foros.id = foros_comentarios.id_foro AND foros_comentarios.id_usuario = usuarios.id WHERE foros.id='".$foro."' AND foros_comentarios.publicado='si' ORDER BY foros_comentarios.fecha_publicacion LIMIT " . $inicio . "," . $TAMANO_PAGINA;
				$query="SELECT f.id, f.titulo AS titulo, f.publicado, f.fecha_publicacion AS fecha_publicacion_foro, fc.id, fc.id_foro, fc.id_usuario, fc.comentario AS foros_comentarios_comentario, fc.publicado, fc.fecha_publicacion AS fecha_publicacion_comentario, u.id AS identificacion_usuario, u.nombre AS nombre_usuario, u.apellidos AS apellidos_usuario, u.email FROM foros AS f INNER JOIN foros_comentarios AS fc INNER JOIN usuarios AS u ON f.id = fc.id_foro AND fc.id_usuario = u.id WHERE f.id='".$foro."' AND fc.publicado='si' ORDER BY fc.fecha_publicacion LIMIT " . $inicio . "," . $TAMANO_PAGINA;
				$result=mysql_query($query);
				if ($num_total_registros > 0) {
					while ($row=mysql_fetch_array($result)) {
					?>
					<article>
						<div class="usuario">
							<p class="user"><?php echo utf8_encode($row["nombre_usuario"]." ".$row["apellidos_usuario"]); ?></p>
							<p class="date"><?php echo fecha($row["fecha_publicacion_comentario"]); ?></p>
						</div>
						<div class="comentario"><?php echo utf8_encode($row["foros_comentarios_comentario"]); ?></div>
					</article>
					<?php
					}
				} else {
				echo "<article><div class=\"sincomentarios\"><p>No hay comentarios en el foro seleccionado</p></div></article>";
				}
				?>

				<?php
				if ($_SESSION["entrar"] == "SI") {
					//extraemos el id del usuario
					$query_usuario = "SELECT id from usuarios WHERE id=".$_SESSION["id_usuario"];
					$result_usuario=mysql_query($query_usuario);
					if ($row_usuario=mysql_fetch_array($result_usuario)) {
						$identificacion_usuario = $row_usuario["id"];
					}
				?>
					<div class="comentariosdelforo">
					<?php
					$mensajeenviado = $_GET["mensaje"];
					switch ($mensajeenviado) {
						case '2':
							$mensaje = "Has introducido un comentario en el foro. Gracias";
							break;
						case '3':
							$mensaje = "Se ha producido un error";
							break;
						default:
							$mensaje = "";
							break;
					}
					echo "<p>".$mensaje."</p>";
					if ($_POST["msg"]=="" && (!isset($_GET["mensaje"]))) {
						$msg = "Puedes incluir un comentario";
					} else {
						$msg = $_POST["msg"];
					}
					echo "<p>".$msg."</p>";
					?>
					</div>

					<div class="comentariosdelforo formulario">
						<form method="post" name="comentarios_foro_usuario" id="comentarios_foro_usuario" action="comentarios_foro_usuario.php?foro=<?php echo $foro; ?>&pagina=<?php echo $pagina; ?>&pageforo=<?php echo $pageforo; ?>">
							<input type="hidden" name="identificacion_usuario" id="identificacion_usuario" value="<?php echo $identificacion_usuario;  ?>" />
							<textarea id="comentariosforo" name="comentariosforo"></textarea>
							<button type="submit">Comentar</button>
						</form>
					</div>
				<?php } else { ?>
					<div class="comentariosdelforo">
						<p>Debes estar <a href="registro.php" title="Ir a la página de registro">registrado</a> para poder realizar comentarios en el foro. Si ya tienes cuenta de usuario deberás <strong>validarte</strong> para poder hacer comentarios.</p>
					</div>
				<?php } ?>
			</section>

			<div id="paginacion">
				<ul>
				<?php
				//muestro los distintos índices de las páginas, si es que hay varias páginas
				if ($total_paginas > 1){

					// PRIMERA PÁGINA
					if ($pagina > 1) {
						echo "<li><a class=\"especial\" title=\"Primera página\" href='".$_SERVER['PHP_SELF']."?pagina=1&foro=".$foro."&pageforo=".$pageforo."'>&laquo;&laquo;</a></li>";
					} else {
						echo "<li class=\"desactivado\">&laquo;&laquo;</li>";
					}

					// PÁGINA ANTERIOR
					if ($pagina > 1) {
						echo "<li><a title=\"Página anterior\" href='".$_SERVER['PHP_SELF']."?pagina=" . ($pagina - 1) ."&foro=".$foro."&pageforo=".$pageforo."'>Anterior</a></li>";
					} else {
						echo "<li class=\"desactivado\">Anterior</li>";
					}

					// RANGO (muestra en la izquierda ..)
					if ($lrange > 1) {
						echo "<li>..</li>";
					} else {
						echo "";
					}

					// PINTA TODAS LAS PÁGINAS INDICANDO LA ACTIVA
					for ($i=1;$i<=$total_paginas;$i++) {
						if ($pagina == $i) {
							//si muestro el índice de la página actual, no coloco enlace
							echo "<li class=\"paginate sel\">".$pagina . "</li>";
						} else {
							//si el índice no corresponde con la página mostrada actualmente, coloco el enlace para ir a esa página
							if ($lrange <= $i && $i <= $rrange) {
								echo "<li class=\"paginate\"><a class=\"navegacion\" title=\"Página $i\" href='".$_SERVER['PHP_SELF']."?pagina=" . $i . "&foro=".$foro."&pageforo=".$pageforo."'>" . $i . "</a></li>";
							}
						}
					}

					// RANGO (muestra en la derecha ..)
					if ($rrange < $total_paginas) {
						echo "<li>..</li>";
					} else {
						echo "";
					}

					// PÁGINA SIGUIENTE
					if ($pagina < $total_paginas) {
						echo "<li><a title=\"Página siguiente\" href='".$_SERVER['PHP_SELF']."?pagina=" . ($pagina + 1) . "&foro=".$foro."&pageforo=".$pageforo."'>Siguiente</a></li>";
					} else {
						echo "<li class=\"desactivado\">Siguiente</li>";
					}

					// ÚLTIMA PÁGINA
					if ($pagina < $total_paginas) {
						echo "<li><a class=\"especial\" title=\"Última página\" href='".$_SERVER['PHP_SELF']."?pagina=" . $total_paginas . "&foro=".$foro."&pageforo=".$pageforo."'>&raquo;&raquo;</a></li>";
					} else {
						echo "<li class=\"desactivado\">&raquo;&raquo;</li>";
					}

				} else {
					echo "<li>&nbsp;</li>";
				}
				?>
				</ul>
			</div>
			<div class="navegacion">
				<ul>
					<li class="anterior"><a href="foros.php?pagina=<?php echo $pageforo; ?>">Volver</a></li>
				</ul>
			</div>
			<a href="#" class="scrolltop2"></a>
		</section>
	</section>
</div>

</body>
</html>

