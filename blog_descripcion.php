<?php
header('Content-Type: text/html; charset=UTF-8');
include("inc/funciones.php");
$pagina = $_GET["pagina"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Blog</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="blog">
			<section id="bloger">
				<?php
				$query="SELECT * FROM blog WHERE id=".$_GET["id_blog"];
				$result=mysql_query($query);
				while ($row=mysql_fetch_array($result)) {
				?>
				<article class="foro">
					<figure>
						<img src="images/blog/secciones/<?php echo $row["imagen"]; ?>" alt="<?php echo utf8_encode($row["titulo"]); ?>" />
						<figcaption>Blog</figcaption>
					</figure>
					<header>
						<h2><?php echo utf8_encode($row["titulo"]); ?></h2>
					</header>
					<div class="textos">
						<?php echo $row["textos"]; ?>
					</div>
					<footer><?php echo "Publicado: ".fecha($row["fecha_publicacion"]); ?></footer>
				</article>
				<?php
				}
				?>

				<div class="navegacion">
					<ul>
						<?php if ($pagina) { ?>
							<li class="anterior"><a href="blog.php?pagina=<?php echo $pagina; ?>" title="volver">volver</a></li>
						<?php } else { ?>
							<li class="anterior"><a href="blog.php" title="volver">volver</a></li>
						<?php } ?>
					</ul>
				</div>

			</section>

			<aside>

				<div id="ultimos">
					<ul id="actuales">
						<li><a href="#">Últimas 10 entradas</a>
							<ul>
							<?php
							$query="SELECT * FROM blog WHERE id!=".$_GET["id_blog"]." ORDER BY fecha_publicacion DESC LIMIT 10";
							$result=mysql_query($query);
							while ($row=mysql_fetch_array($result)) {
							?>
								<li><a href="blog_descripcion.php?id_blog=<?php echo $row["id"]; ?>"><?php echo utf8_encode($row["titulo"]); ?></a></li>
							<?php
							}
							?>
							</ul>
						</li>
					</ul>
				</div>

				<div id="historico">
					<h3>Más actuales</h3>
					<ul id="historial">
						<?php
						$anio_actual = date("Y");
						$mes_actual = date("m");
						$query="SELECT DISTINCT YEAR(fecha_publicacion) AS anio FROM blog WHERE publicado='si' ORDER BY fecha_publicacion DESC";
						$result=mysql_query($query);
						while ($row=mysql_fetch_array($result)) {
						?>
						<li class="anio<?php if ($anio_actual == $row["anio"]) { echo " sel"; } ?>"><a href="#"><?php echo $row["anio"];?></a>
							<?php
							$query1="SELECT DISTINCT MONTH(fecha_publicacion) AS mes FROM blog WHERE publicado='si' AND YEAR(fecha_publicacion)=".$row["anio"]." ORDER BY fecha_publicacion DESC";
							$result1=mysql_query($query1);
							if ($row1=mysql_fetch_array($result1)) {
							?>
								<ul class="meses<?php if (($anio_actual == $row["anio"]) && ($mes_actual == $row1["mes"])) { echo " mostrar"; } ?>">
									<?php
									$query2="SELECT DISTINCT MONTH(fecha_publicacion) AS mes FROM blog WHERE publicado='si' AND YEAR(fecha_publicacion)=".$row["anio"]." ORDER BY fecha_publicacion DESC";
									$result2=mysql_query($query2);
									while ($row2=mysql_fetch_array($result2)) {
									?>
									<li class="mes<?php if (($anio_actual == $row["anio"]) && ($mes_actual == $row2["mes"])) { echo " sel"; } ?>"><a href="#"><?php echo pintarMes($row2["mes"]); ?></a>
										<ul class="unmes<?php if (($anio_actual == $row["anio"]) && ($mes_actual == $row2["mes"])) { echo " mostrar"; } ?>">
										<?php
											$anio_p = $row["anio"];
											$mes_p = $row2["mes"];
											$query3="SELECT id, titulo FROM blog WHERE publicado='si' AND YEAR(fecha_publicacion)='". $anio_p ."' AND MONTH(fecha_publicacion)='".$mes_p."' ORDER BY fecha_publicacion DESC";
											$result3=mysql_query($query3);
											while ($row3=mysql_fetch_array($result3)) {
											?>
												<li><a href="blog_descripcion.php?id_blog=<?php echo $row3["id"]; ?>"><?php echo utf8_encode($row3["titulo"]); ?></a></li>
										<?php
										}
										?>
										</ul>
									</li>
									<?php
									}
									?>
								</ul>
							<?php
							}
							?>
						</li>
						<?php
						}
						mysql_close($link);
						?>
					</ul>
				</div>
			</aside>

		</section>
	</section>
</div>

</body>
</html>


