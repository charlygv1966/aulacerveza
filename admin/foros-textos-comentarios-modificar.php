<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
	$foro = $_GET["foro"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>FOROS (Comentarios)</h1>
		<h2><?php echo fecha(); ?></h2>
		<h3><strong>Si deseas que algun comentario no sea visible puedes despublicarlo (no se borrará).</strong></h3>
		<h3>El enlace de <em>Publicar/Despublicar</em> funciona como un interruptor publicando/despublicando el comentario.</h3>
		<?php
		$query="SELECT titulo FROM foros WHERE id=".$foro;
		$result=mysql_query($query);
		while ($row=mysql_fetch_array($result)) {
		?>
		<h3>FORO: <strong><?php echo utf8_encode($row["titulo"]); ?></strong></h3>
		<?php
		}
		?>
		<table summary="Listado de foros publicados" class="contactos">
		<thead>
			<tr>
				<th>Comentario</th>
				<th>Fecha</th>
				<th>Usuario</th>
				<th>Publicado</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
			//$query="SELECT foros.id, foros.titulo AS titulo, foros.publicado, foros.fecha_publicacion AS fecha_publicacion_foro, foros_comentarios.id AS foros_comentarios_comentario_id, foros_comentarios.id_foro, foros_comentarios.id_usuario, foros_comentarios.comentario AS foros_comentarios_comentario, foros_comentarios.publicado AS foros_comentarios_comentario_publicado, foros_comentarios.fecha_publicacion AS fecha_publicacion_comentario, usuarios.id AS identificacion_usuario, usuarios.nombre AS nombre_usuario, usuarios.apellidos AS apellidos_usuario, usuarios.email FROM foros INNER JOIN foros_comentarios INNER JOIN usuarios ON foros.id = foros_comentarios.id_foro AND foros_comentarios.id_usuario = usuarios.id WHERE foros.id='".$foro."' ORDER BY foros_comentarios.fecha_publicacion";
			$query="SELECT f.id, f.titulo AS titulo, f.publicado, f.fecha_publicacion AS fecha_publicacion_foro, fc.id AS foros_comentarios_comentario_id, fc.id_foro, fc.id_usuario, fc.comentario AS foros_comentarios_comentario, fc.publicado AS foros_comentarios_comentario_publicado, fc.fecha_publicacion AS fecha_publicacion_comentario, u.id AS identificacion_usuario, u.nombre AS nombre_usuario, u.apellidos AS apellidos_usuario, u.email FROM foros AS f INNER JOIN foros_comentarios AS fc INNER JOIN usuarios AS u ON f.id = fc.id_foro AND fc.id_usuario = u.id WHERE f.id='".$foro."' ORDER BY fc.fecha_publicacion";
			$result=mysql_query($query);
			$total = mysql_num_rows($result);
			while ($row=mysql_fetch_array($result)) {
			?>
			<tr>
				<td><?php echo utf8_encode($row["foros_comentarios_comentario"]); ?></td>
				<td><?php echo fecha_reserva($row["fecha_publicacion_comentario"]); ?></td>
				<td><?php echo utf8_encode($row["nombre_usuario"]." ".$row["apellidos_usuario"])." - (".$row["identificacion_usuario"].")"; ?></td>
				<td class="centrado">
				<select name="publicado" id="publicado" disabled="disabled">
					<option value="si" <?php if ($row["foros_comentarios_comentario_publicado"] == "si") { echo "selected=\"selected\""; } ?>>Si</option>
					<option value="no" <?php if ($row["foros_comentarios_comentario_publicado"] == "no") { echo "selected=\"selected\""; } ?>>No</option>
				</select>
				</td>
				<td><a href="foros-textos-comentarios-actualizar.php?foro=<?php echo $foro; ?>&comentario=<?php echo $row["foros_comentarios_comentario_id"]; ?>&publicado=<?php echo $row["foros_comentarios_comentario_publicado"]; ?>" title="Despublicar">Publicar / Despublicar</a></td>
			</tr>
			<?php
			}
			?>
			<tr>
				<td colspan="5" class="totales">Total comentarios del foro: <?php echo $total; ?></td>
			</tr>
		</tbody>
		</table>
		<button type="button" id="volverforos" name="volverforos">VOLVER</button>
	</div>
</div>
</body>

</html>
