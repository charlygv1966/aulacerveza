<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
	$curso = $_GET["curso"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>CURSOS (Reservas de cursos)</h1>
		<h2><?php echo fecha(); ?></h2>
		<?php
		$query = "SELECT titulo FROM cursos WHERE id=".$curso;
		$result=mysql_query($query);
		while ($row=mysql_fetch_array($result)) {
		?>
		<h3>CURSO: <strong><?php echo utf8_encode($row["titulo"]); ?></strong></h3>
		<?php
		}
		?>
		<table summary="Listado de personas con reserva en el curso" class="contactos">
		<thead>
			<tr>
				<th>Nombre del Curso</th>
				<th>Persona que ha hecho la reserva</th>
				<th>Fecha de la reserva</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$query = "SELECT cursos.id, cursos.titulo AS cursotitulo, cursos_reservas.id_curso AS id_curso, cursos_reservas.persona_reserva AS persona_reserva, cursos_reservas.fecha_reserva AS fecha_reserva FROM cursos INNER JOIN cursos_reservas ON cursos.id = cursos_reservas.id_curso WHERE id_curso=".$curso;
		$result=mysql_query($query);
		$total = mysql_num_rows($result);
		if ($total > 0) {
			while ($row=mysql_fetch_array($result)) {
			?>
			<tr>
				<td><?php echo utf8_encode($row["cursotitulo"]); ?></td>
				<td><?php echo utf8_encode($row["persona_reserva"]); ?></td>
				<td><?php echo fecha_reserva($row["fecha_reserva"]); ?></td>
			</tr>
			<?php
			}
			?>
			<tr>
				<td colspan="3" class="totales">Total reservas: <?php echo $total; ?></td>
			</tr>
		<?php
		} else {
		?>
		<tr>
			<td colspan="3">No hay reservas en este curso</td>
		</tr>
		<?php
		}
		?>
		</tbody>
		</table>
		<button type="submit" id="volver" name="volver">VOLVER</button>
	</div>
</div>
</body>

</html>
