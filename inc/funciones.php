<?php
function normalizacion($txt){
	$texto = array("`","'","\"","<",">");
	$textoreplace = array("\'","\'","\"","&lt;","&gt;");
	return str_replace($texto,$textoreplace,$txt);
}

function normalizacion_textos($txt){
	$texto = array("`","'","\"","<script","script>");
	$textoreplace = array("\'","\'","\"","&lt;script","script&gt;");
	return str_replace($texto,$textoreplace,$txt);
}

function texto_checks($txt){
	$texto = array(" ","/");
	$textoreplace = array("","");
	return str_replace($texto,$textoreplace,$txt);
}

function texto_descripcion($txt){
	$texto = array("<p>","</p>","<P>","</P>");
	$textoreplace = array("","","","");
	return str_replace($texto,$textoreplace,$txt);
}

function texto_amp($txt){
	$texto = array("&");
	$textoreplace = array("&amp;");
	return str_replace($texto,$textoreplace,$txt);
}

function esVacio($val) {
    if($val=="")
		return 1;
	else
		return 0;
}

function esLargo($val) {
	return strlen ($val);
}

function esEmail($val) {
    $arroba = strpos($val,'@');
    if ($arroba ===false){
        return 0;
	}
    else {
    $punto = strpos($val,'.',$arroba);
      if ($punto < $arroba + 2 || punto > strlen(val) - 2)
         return 0;
   }
   return 1;
}

function esTelefono($val) {
	if (preg_match('/^1?[0-9]{9}$/', $val) && ((substr($val,0,1)=='6') || (substr($val,0,1)=='7') || (substr($val,0,1)=='8') || (substr($val,0,1)=='9')))
  		return 0;
  	else
  		return 1;
}

function esCP($val) {
	if ((eregi("^[0-9]{5}$", $val)) && ($val>'01000' && $val<='52080'))
		return 0;
	else
		return 1;
}

function esNumero($val) {
	if (!is_numeric($val))
  		return 0;
  	else
  		return 1;
}

function esNIF($val) {
   $val = strtoupper($val);
   for ($i = 0; $i < 9; $i ++)
      $num[$i] = substr($val, $i, 1);
//si no tiene un formato valido devuelve error
   if (!ereg('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)', $val))
      return 0;
//comprobacion de NIFs estandar
   if (ereg('(^[0-9]{8}[A-Z]{1}$)', $val))
      if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKET', substr($val, 0, 8) % 23, 1))
         return 1;
      else
         return 0;
//algoritmo para comprobacion de codigos tipo NIF
   $suma = $num[2] + $num[4] + $num[6];
   for ($i = 1; $i < 8; $i += 2)
      $suma += substr((2 * $num[$i]),0,1) + substr((2 * $num[$i]),1,1);
   $n = 10 - substr($suma, strlen($suma) - 1, 1);
//comprobacion de NIFs especiales (se calculan como CIFs o como NIFs)
   if (ereg('^[KLM]{1}', $val))
      if ($num[8] == chr(64 + $n) || $num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($val, 1, 8) % 23, 1))
         return 1;
      else
         return 0;
//si todavia no se ha verificado devuelve error
   return 0;
}

function esNIFNIE($cif) {
//Copyright 2005-2008 David Vidal Serra. Bajo licencia GNU GPL.
//Este software viene SIN NINGUN TIPO DE GARANTIA; para saber mas detalles
//puede consultar la licencia en http://www.gnu.org/licenses/gpl.txt(1)
//Esto es software libre, y puede ser usado y redistribuirdo de acuerdo
//con la condicion de que el autor jamas sera responsable de su uso.
//Returns: 1 = NIF ok, 2 = CIF ok, 3 = NIE ok, -1 = NIF bad, -2 = CIF bad, -3 = NIE bad, 0 = ??? bad
   $cif = strtoupper($cif);
   for ($i = 0; $i < 9; $i ++)
      $num[$i] = substr($cif, $i, 1);
//si no tiene un formato valido devuelve error
   if (!ereg('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)', $cif))
      return 0;
//comprobacion de NIFs estandar
   if (ereg('(^[0-9]{8}[A-Z]{1}$)', $cif))
      if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKET', substr($cif, 0, 8) % 23, 1))
         return 1;
      else
         return 0;
//comprobacion de NIEs
   //T
   if (ereg('^[T]{1}', $cif))
      if ($num[8] == ereg('^[T]{1}[A-Z0-9]{8}$', $cif))
         return 1;
      else
         return 0;
   //XYZ
   if (ereg('^[XYZ]{1}', $cif))
      if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKET', substr(str_replace(array('X','Y','Z'), array('0','1','2'), $cif), 0, 8) % 23, 1))
         return 1;
      else
         return 0;
//si todavia no se ha verificado devuelve error
   return 0;
}

function fecha($fecha_actual) {
	return substr($fecha_actual,8,2)."/".substr($fecha_actual,5,2)."/".substr($fecha_actual,0,4)." a las ".substr($fecha_actual,11,8);
}

function fecha_foro($fecha_actual) {
	return substr($fecha_actual,8,2)."/".substr($fecha_actual,5,2)."/".substr($fecha_actual,0,4);
}

function pintarMes($mes) {
	switch ($mes) {
		case 1:
			$pintaMes = "Enero";
			break;
		case 2:
			$pintaMes = "Febrero";
			break;
		case 3:
			$pintaMes = "Marzo";
			break;
		case 4:
			$pintaMes = "Abril";
			break;
		case 5:
			$pintaMes = "Mayo";
			break;
		case 6:
			$pintaMes = "Junio";
			break;
		case 7:
			$pintaMes = "Julio";
			break;
		case 8:
			$pintaMes = "Agosto";
			break;
		case 9:
			$pintaMes = "Septiembre";
			break;
		case 10:
			$pintaMes = "Octubre";
			break;
		case 11:
			$pintaMes = "Noviembre";
			break;
		case 12:
			$pintaMes = "Diciembre";
			break;
		default:
			$pintaMes = "";
			break;
	}
	return $pintaMes;
}

/* función auxiliar para los contenidos de los blogs */
function cortaStrings($text, $longitud, $html=false) {
	$final='';
	$total=0;

	foreach (explode(' ',$text) as $word) {
		if ($word != '') {
			$final .= ' ' . $word;
			$total += strlen($word);
		}

		if ($total >= $longitud) {
			$final .= '...';
			$tags_apertura = "%((?<!</)(?<=<)[\s]*[^/!>\s]+(?=>|[\s]+[^>]*[^/]>)(?!/>))%";
			$tags_cierre = "|</([a-zA-Z]+)>|";

			if (preg_match_all($tags_apertura,$final,$aBuffer)) {
				if (!empty($aBuffer[1])) {
					preg_match_all($tags_cierre,$final,$aBuffer2);
					if (count($aBuffer[1]) != count($aBuffer2[1])) {

						$aBuffer[1] = array_reverse($aBuffer[1]);

						foreach ($aBuffer[1] as $index => $tag) {
							if (empty($aBuffer2[1][$index]) || $aBuffer2[1][$index] != $tag)
								$final .= '</'.$tag.'>';
						}
					}
				}
			}
			break;
		}
	}
	return $final;
}

?>
