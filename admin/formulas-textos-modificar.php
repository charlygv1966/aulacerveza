<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>FÓRMULAS</h1>
		<?php
		$textos = trim($_POST["formulastextos"]);
		$envio=1;

		if (esVacio($textos)) {
			$envio=0;
		}

		if ($envio) {
			$query = "UPDATE formulas SET textos='$textos',fecha_actualizacion=now()";
			$result=mysql_query($query);
			mysql_close($link);
				if ($result){
				?>
				<script type="text/javascript">
					document.location.href="formulas-textos.php?mensaje=ok";
				</script>
				<?php
				} else {
				?>
				<script type="text/javascript">
					document.location.href="formulas-textos.php?mensaje=error";
				</script>
				<?php
				}
		} else {
		?>
		<script type="text/javascript">
			document.location.href="formulas-textos.php?mensaje=error";
		</script>
		<?php
		}
		?>
	</div>
</div>
</body>

</html>
