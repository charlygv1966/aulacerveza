<?php
header('Content-Type: text/html; charset=UTF-8');
include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Videos</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="videos">
			<?php
			$query="SELECT * FROM videos ORDER BY fecha_actualizacion";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
			<article>
				<figure>
					<img src="images/videos/<?php echo $row["imagen"]; ?>" alt="Videos" />
					<figcaption>Videos</figcaption>
				</figure>
				<div class="textos"><?php echo $row["textos"]; ?></div>
				<div class="youtube">
					<p><a href="<?php echo $row["youtube"]; ?>" title="Visita Canal Youtube">Visita Canal Youtube</a></p>
				</div>
			</article>
			<?php
			}
			mysql_close($link);
			?>
		</section>
	</section>
</div>

</body>
</html>

