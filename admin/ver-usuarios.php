<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>LISTADO DE USUARIOS (usuarios que se han registrado en la web)</h1>
		<h2><?php echo fecha(); ?></h2>
		<h3>Los usuarios que se registran están activados por defecto. Si quieres desactivar a algun usuario por cualquier motivo, puedes hacerlo marcando la casilla <strong>Activar / Desactivar</strong> que funciona como un interruptor activando o desactivando segun cual sea la opción actual.</h3>

		<table summary="Listado de usuarios registrados" class="contactos">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Apellidos</th>
				<th>Email</th>
				<th>País</th>
				<th colspan="2" class="centrado">Activado</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$query="SELECT * FROM usuarios ORDER BY id";
		$result=mysql_query($query);
		$total = mysql_num_rows($result);
		while ($row=mysql_fetch_array($result)) {
		?>
			<tr>
				<td><?php echo $row["id"]; ?></td>
				<td><?php echo utf8_encode($row["nombre"]); ?></td>
				<td><?php echo utf8_encode($row["apellidos"]); ?></td>
        <?php if ($row["activado"] == "si") { ?>
				  <td><a href="mailto:<?php echo utf8_encode($row['email']); ?>?subject=Mensaje personalizado para usuarios de AULA CERVEZA&body=Hola <?php echo utf8_encode($row["nombre"]); ?>." title="Enviar un correo"><?php echo utf8_encode($row['email']); ?></a></td>
        <?php } else { ?>
          <td><?php echo utf8_encode($row["email"]); ?></td>
        <?php } ?>
				<td><?php echo utf8_encode($row["pais"]); ?></td>
				<td class="centrado">
				<select name="activado" id="activado" disabled="disabled">
					<option value="si" <?php if ($row["activado"] == "si") { echo "selected=\"selected\""; } ?>>Si</option>
					<option value="no" <?php if ($row["activado"] == "no") { echo "selected=\"selected\""; } ?>>No</option>
				</select>
				</td>
				<td><a href="ver-usuarios-actualizar.php?usuario=<?php echo $row["id"]; ?>&activado=<?php echo $row["activado"]; ?>" title="Activar/Desactivar">Activar/Desactivar</a></td>
			</tr>
		<?php
		}
		?>
			<tr>
				<td colspan="7" class="totales">Total usuarios: <?php echo $total; ?></td>
			</tr>
		<?php
		mysql_close($link);
		?>
		</tbody>
		</table>
	</div>
</div>
</body>

</html>
