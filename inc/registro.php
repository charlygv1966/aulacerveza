<?php
error_reporting(0);
ini_set('display_errors','0');
session_start();
if ($_SESSION["entrar"] == "SI") {
?>
	<div class="usuarioregistrado">
		<ul>
			<li><span>Bienvenid@</span> <strong><?php echo utf8_encode($_SESSION["nombre"]); ?></strong></li>
		</ul>
		<ul class="modificar">
			<li><a href="modificar.php" title="Modificar datos">Modificar datos</a></li>
			<li>|</li>
			<li><a href="inc/cerrar.php" title="Cerrar sesión">Cerrar&nbsp;sesión</a></li>
		</ul>
	</div>
<?php
} else {
?>
	<form method="post" action="inc/validar.php" id="formregistro">
		<div class="formulario">
			<label for="usuario">Usuario</label>
			<input type="text" name="usuario" id="usuario" placeholder="usuario" />
			<label for="pass">Contraseña</label>
			<input type="password" name="pass" id="pass" placeholder="password" />
		</div>
		<div class="formulario">
			<button type="button" name="ok" id="ok">OK</button>
			<button type="button" name="registrarse" id="registrarse">Regístrate</button>
			<p><a href="recordar.php">¿Olvidaste tu contraseña?</a></p>
		</div>
	</form>
<?php
}
?>
