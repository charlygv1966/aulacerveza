<?php
header('Content-Type: text/html; charset=UTF-8');
include("admin/inc/conexion.php");
include("inc/funciones.php");
require ("mail/class.phpmailer.php");
$lang="español";
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Contacto</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<?php
$contacto_email = $_POST["contacto_email"];
$nombre=trim($_POST["nombre"]);
$email=trim($_POST["email"]);
$telefono=trim($_POST["telefono"]);
$asunto=trim($_POST["asunto"]);
$mensaje=trim($_POST["mensaje"]);
$envio=1;

if (esVacio($nombre)) {
	$envio=0;
	$msg="Introduce tu nombre";
} else if(esVacio($email)){
	$envio=0;
	$msg="Introduce tu email";
} else if(!esVacio($email) && !esEmail($email)){
	$envio=0;
	$msg="Introduce un email v&aacute;lido";
} else if(esVacio($asunto)){
	$envio=0;
	$msg="Debes indicar el asunto del mensaje";
} else if(esVacio($mensaje)){
	$envio=0;
	$msg="Debes indicar el mensaje";
}

//si envio OK
if ($envio) {
	try {
		$mail = new PHPMailer(true); //New instance, with exceptions enabled

		/*$body="AULA CERVEZA. Contacto enviado:"."\n\n";
		$body.="Idioma: ".$lang."\n\n";
		$body.="Nombre: ".$_POST['nombre']."\n";
		$body.="Email: ".$_POST['email']."\n";
		$body.="Teléfono: ".$_POST['telefono']."\n";
		$body.="Asunto: ".$_POST['asunto']."\n";
		$body.="Mensaje: ".$_POST['mensaje']."\n";
		$body=utf8_decode($body);*/

		$body="AULA CERVEZA. Contacto enviado:"."\n\n";
		$body.="Nombre: ".$nombre."\n";
		$body.="Email: ".$email."\n";
		$body.="Teléfono: ".$telefono."\n";
		$body.="Asunto: ".$asunto."\n";
		$body.="Mensaje: ".$mensaje."\n";
		$body=utf8_decode($body);

		$mail->SetLanguage('es');
		$mail->IsSMTP();                           		// tell the class to use SMTP
		$mail->SMTPAuth   = true;                 		// enable SMTP authentication
		$mail->Port       = 587;                   		// set the SMTP server port
		$mail->Host       = "127.0.0.1"; 				// SMTP server
		//$mail->Username = "contacto@aulacerveza.com"; // SMTP server username
		$mail->Username   = $contacto_email;			// selecciono el dato de la base de datos
		$mail->Password   = "password";            		// SMTP server password
		//$mail->From     = "contacto@aulacerveza.com";
		$mail->From       = $contacto_email;			// selecciono el dato de la base de datos
		$mail->FromName   = "Aula Cerveza";
		//$to = "contacto@aulacerveza.com"; 			// este email debo sacarlo de la base de datos contacto
		$to = $contacto_email;							// selecciono el dato de la base de datos
		$mail->AddAddress($to);
		$mail->Subject  = "Solicitud de informacion de Aula Cerveza";
		$mail->AltBody    = ""; 						// optional, comment out and test
		$mail->WordWrap   = 80; 						// set word wrap
		$mail->MsgHTML($body);
		$mail->IsHTML(true); 							// send as HTML
		$mail->Send(); ?>
		<script type="text/javascript">
			document.location.href="contacto.php?mensaje=2";
		</script>
		<?php
	} catch (phpmailerException $e) { ?>
		<script type="text/javascript">
			document.location.href="contacto.php?mensaje=3";
		</script>
		<?php
	}
	// como el correo de momento no funciona lo que hago es grabarlo en base de datos
	//$query="INSERT INTO contactos (nombre, email, telefono, asunto, mensaje, fecha_contacto) VALUES ('". utf8_decode(normalizacion($nombre))."','".utf8_decode(normalizacion($email))."','".utf8_decode(normalizacion($telefono))."','".utf8_decode(normalizacion($asunto))."','".utf8_decode(normalizacion($mensaje))."','".now()."')";
	$query="INSERT INTO contactos (nombre, email, telefono, asunto, mensaje, fecha_contacto) VALUES ('". utf8_decode(normalizacion($nombre))."','".utf8_decode(normalizacion($email))."','".utf8_decode(normalizacion($telefono))."','".utf8_decode(normalizacion($asunto))."','".utf8_decode(normalizacion($mensaje))."',now())";
	$result2=mysql_query($query);
	if ($result2){ ?>
		<script type="text/javascript">
			document.location.href="contacto.php?mensaje=2";
		</script>
	<?php } else { ?>
		<script type="text/javascript">
			document.location.href="contacto.php?mensaje=3";
		</script>
<?php }
} else {
?>
<form method="post" action="contacto.php" id="form" name="form" accept-charset="utf-8">
<input type="hidden" name="nombre" value="<?php echo $nombre; ?>" />
<input type="hidden" name="email" value="<?php echo $email; ?>" />
<input type="hidden" name="telefono" value="<?php echo $telefono; ?>" />
<input type="hidden" name="asunto" value="<?php echo $asunto; ?>" />
<input type="hidden" name="mensaje" value="<?php echo $mensaje; ?>" />
<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
</form>
<script type="text/javascript">
	document.form.submit();
</script>
<?php } ?>

</body>
</html>