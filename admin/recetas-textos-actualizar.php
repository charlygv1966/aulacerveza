<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>RECETAS (Modificar contenidos)</h1>
		<?php
		if ($_POST) {
			$id_receta = $_POST["id_receta"];
			$titulo = trim(utf8_decode(normalizacion_textos($_POST["titulo"])));
			$recetatextos = trim($_POST["recetatextos"]);
			$publicado = trim($_POST["publicado"]);
			$envio=1;
		}

		if (esVacio($titulo)) {
			$envio=0;
			$msg="Introduce el título del artículo";
		} else if(esVacio($recetatextos)){
			$envio=0;
			$msg="Introduce el contenido del artículo";
		}

		if ($envio) {
			$query = "UPDATE recetas SET titulo='$titulo',textos='$recetatextos',publicado='$publicado',fecha_actualizacion=now() WHERE id=".$id_receta;
			$result=mysql_query($query);
			mysql_close($link);
				if ($result){
				?>
				<script type="text/javascript">
					document.location.href="recetas-textos-modificar.php?mensaje=2&receta=<?php echo $id_receta; ?>";
				</script>
				<?php
				} else {
				?>
				<script type="text/javascript">
					document.location.href="recetas-textos-modificar.php?mensaje=3&receta=<?php echo $id_receta; ?>";
				</script>
				<?php
				}
		} else {
		?>
		<form method="post" action="recetas-textos-modificar.php?receta=<?php echo $id_receta; ?>" id="form" name="form" accept-charset="utf-8">
			<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
		</form>
		<script type="text/javascript">
			document.form.submit();
		</script>
		<?php
		}
		?>
	</div>
</div>
</body>

</html>
