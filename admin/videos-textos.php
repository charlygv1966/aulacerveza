<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>VIDEOS (Textos)</h1>
		<h2><?php echo fecha(); ?></h2>
		<form method="post" action="videos-textos-modificar.php" accept-charset="utf-8">
			<?php
			$query="SELECT * FROM videos ORDER BY fecha_actualizacion";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
				<textarea id="videostextos" name="videostextos"><?php echo $row["textos"]; ?></textarea>
				<p class="videos">
					<label for="youtube">Dirección en Youtube:</label>
					<input type="text" name="youtube" id="youtube" placeholder="Introduce la dirección de Aula cerveza en Youtube" value="<?php echo $row["youtube"]; ?>" />
				</p>
				<button type="reset" id="cancelar" name="cancelar">VOLVER</button>
				<button type="submit" id="videos" name="videos">MODIFICAR TEXTO</button>
			<?php
			}
			?>
		</form>
		<div class="mensajes">
			<?php if ($_GET["mensaje"] == "ok") {
				echo "<p>Has modificado el contenido de la sección</p>";
			} elseif ($_GET["mensaje"] == "error") {
				echo "<p>Se ha producido un error</p>";
			} else {
				echo "<p>&nbsp;</p>";
			}
			?>
		</div>
	</div>
</div>
</body>

</html>
