<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
	include("inc/fecha.php");
	$seccion = $_GET["seccion"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>SECCIONES PRINCIPALES DE LA WEB</h1>
		<h2><?php echo fecha(); ?></h2>
		<h3>SECCIONES</h3>
		<form method="post" accept-charset="utf-8" action="secciones-actualizar.php">
			<?php
			$query="SELECT * FROM menu WHERE id=".$_GET["seccion"];
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
				<input type="hidden" name="id_seccion" id="id_seccion" value="<?php echo $_GET["seccion"] ?>" />
				<p>
					<label for="titulo">Título de la sección:</label>
					<input type="text" name="nombre" id="nombre" value="<?php echo utf8_encode($row["nombre"]); ?>" readonly="readonly" />
				</p>
				<p>
					<label for="activo">Publicado</label>
					<select name="activo" id="activo">
						<option value="1" <?php if ($row["activo"] == 1) { echo "selected=\"selected\""; } ?>>Si</option>
						<option value="0" <?php if ($row["activo"] == 0) { echo "selected=\"selected\""; } ?>>No</option>
				</select>
				</p>
				<p>
					<button type="reset" id="cancelarsecciones" name="cancelarsecciones">CANCELAR</button>
					<button type="submit" id="secciones" name="secciones">MODIFICAR</button>
				</p>
			<?php
			}
			?>
		</form>
	</div>
</div>
</body>

</html>
