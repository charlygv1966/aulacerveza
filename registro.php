<?php
header('Content-Type: text/html; charset=UTF-8');
include("admin/inc/conexion.php");
include("inc/funciones.php");

if ($_POST) {
	$nombre=$_POST["nombre"];
	$apellidos=$_POST["apellidos"];
	$email=$_POST["email"];
	$pais=$_POST["pais"];
	$password=$_POST["password"];
	$password2=$_POST["password2"];
	$msg=$_POST["msg"];
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Contacto</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/jquery-ui-1.11.1.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery-ui-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="registrogeneral">
			<h2><img src="images/registro/registro.png" alt="Registro"></h2>
			<form method="post" id="formu1ario" accept-charset="utf-8" action="registrar.php">
				<p>
				<label for="nombre">Nombre: *</label>
				<input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre" value="<?php echo $nombre; ?>" />
				</p>
				<p>
				<label for="apellidos">Apellidos: *</label>
				<input type="text" name="apellidos" id="apellidos" placeholder="Introduce tus apellidos" value="<?php echo $apellidos; ?>" />
				</p>
				<p>
				<label for="email">E-Mail: *</label>
				<input type="text" name="email" id="email" placeholder="Introduce tu e-mail" value="<?php echo $email; ?>" />
				</p>
				<p>
				<label for="pais">País: *</label>
				<input type="text" name="pais" id="pais" placeholder="Introduce tu país" value="<?php echo $pais; ?>" />
				</p>
				<p>
				<label for="password">Contraseña: *</label>
				<input type="password" name="password" id="password" placeholder="Introduce tu password" value="<?php echo $password; ?>" />
				</p>
				<p>
				<label for="password2">Verificar contraseña: *</label>
				<input type="password" name="password2" id="password2" placeholder="Introduce tu password otra vez" />
				</p>
				<p class="requeridos">Campos requeridos*</p>
				<button type="submit" id="enviar" name="enviar" value="Enviar">Enviar</button>
				<div class="mensajesregistro">
					<?php include("inc/mensajes-registro.php"); ?>
				</div>
			</form>
		</section>
	</section>
</div>

</body>
</html>

