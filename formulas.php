<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Fórmulas</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="formulas">
			<?php
			$query="SELECT * FROM formulas ORDER BY fecha_actualizacion";
			$result=mysql_query($query);
			while ($row=mysql_fetch_array($result)) {
			?>
			<article>
				<figure>
					<img src="images/formulas/<?php echo $row["imagen"]; ?>" alt="Fórmulas" />
					<figcaption>Fórmulas</figcaption>
				</figure>
				<div class="textos">
					<h2><img src="images/formulas/formulas.png" alt="Fórmulas"></h2>
					<?php echo $row["textos"]; ?>
				</div>
			</article>
			<?php
			}
			mysql_close($link);
			?>
			<div class="navegacion">
				<ul>
					<li class="siguiente"><a href="tusformulas.php">Acceder</a></li>
				</ul>
			</div>

			<div class="mensajes">
				<?php
					$mensajeenviado = $_POST["msg"];
					switch ($mensajeenviado) {
						case '0':
							$mensaje = "<p>Debes estar registrado para acceder a tus formulas</p>";
							break;
						default:
							$mensaje = "";
							break;
					}
					echo $mensaje;
				?>
			</div>

		</section>
	</section>
</div>

</body>
</html>

