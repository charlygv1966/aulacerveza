<?php
	header('Content-Type: text/html; charset=UTF-8');
	include("inc/seguridad.php");
	include("inc/conexion.php");
	include("inc/funciones.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA: Gestor de Contenidos</title>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<script src="js/jquery-1.11.1.js"></script>
<script src="js/jquery-ui-1.11.1.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
<body>
<div id="body-wrapper">
	<div id="sidebar">
		<div id="sidebar-wrapper">
			<?php include("inc/cabecera.php"); ?>
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</div>

	<div id="main-content">
		<h1>CURSOS (Modificar contenidos)</h1>
		<?php
		if ($_POST) {
			$id_curso = $_POST["id_curso"];
			$cursotitulo = trim(utf8_decode(normalizacion_textos($_POST["cursotitulo"])));
			$cursodescripcion = $_POST["cursodescripcion"];
			$cursolugar = $_POST["cursolugar"];
			$cursoplazas = $_POST["cursoplazas"];
			$cursoforma_pago = trim(utf8_decode(normalizacion_textos($_POST["cursoforma_pago"])));
			$cursotipo = $_POST["cursotipo"];
			$cursoprecio = normalizacion_precios($_POST["cursoprecio"]);
			$cursocomienzo = fechacurso($_POST["cursocomienzo"]);
			$cursofinal = fechacurso($_POST["cursofinal"]);
			$cursopublicado = trim($_POST["cursopublicado"]);
			$envio=1;
		}

		if (esVacio($cursotitulo)) {
			$envio=0;
			$msg="Introduce el título del curso";
		} else if(esVacio($cursodescripcion)) {
			$envio=0;
			$msg="Introduce la descripcion del curso";
		} else if(esVacio($cursolugar)) {
			$envio=0;
			$msg="Introduce el lugar de realización del curso";
		} else if(esVacio($cursoplazas)) {
			$envio=0;
			$msg="Debes indicar el número de plazas del curso";
		} else if(esVacio($cursoforma_pago)) {
			$envio=0;
			$msg="Introduce la forma de pago del curso";
		} else if(esVacio($cursotipo)) {
			$envio=0;
			$msg="Selecciona el tipo del curso (PRESENCIAL u ONLINE)";
		} else if(esVacio($cursoprecio)) {
			$envio=0;
			$msg="Introduce el precio del curso";
		} else if(esVacio($cursocomienzo)) {
			$envio=0;
			$msg="Introduce la fecha de comienzo del curso";
		} else if(esVacio($cursofinal)) {
			$envio=0;
			$msg="Introduce la fecha de finalización del curso";
		} else if(fechacurso($cursofinal) < fechacurso($cursocomienzo)) {
			$envio=0;
			$msg="La fecha que has indicado es erronea.<br />La fecha de finalización del curso debe ser igual o mayor que la fecha de comienzo";
		} else if(esVacio($cursopublicado)){
			$envio=0;
			$msg="Debes indicar si quieres publicar o no el curso";
		}

		if ($envio) {
			$query = "UPDATE cursos SET titulo='$cursotitulo',descripcion='$cursodescripcion',lugar_realizacion='$cursolugar',plazas=$cursoplazas,forma_pago='$cursoforma_pago',tipo_curso='$cursotipo',precio_curso=$cursoprecio,fecha_comienzo='$cursocomienzo',fecha_final='$cursofinal',publicado='$cursopublicado' WHERE id=".$id_curso;
			$result=mysql_query($query);
			mysql_close($link);
				if ($result){
				?>
				<script type="text/javascript">
					document.location.href="cursos-textos-modificar.php?mensaje=2&curso=<?php echo $id_curso; ?>";
				</script>
				<?php
				} else {
				?>
				<script type="text/javascript">
					document.location.href="cursos-textos-modificar.php?mensaje=3&curso=<?php echo $id_curso; ?>";
				</script>
				<?php
				}
		} else {
		?>
		<form method="post" action="cursos-textos-modificar.php?curso=<?php echo $id_curso; ?>" id="form" name="form" accept-charset="utf-8">
			<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
		</form>
		<script type="text/javascript">
			document.form.submit();
		</script>
		<?php
		}
		?>
	</div>
</div>
</body>

</html>
