<?php
header('Content-Type: text/html; charset=UTF-8');
include("inc/funciones.php");
$foro = $_GET["foro"];
$pagina = $_GET["pagina"];
$pageforo = $_GET["pageforo"];
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>AULA CERVEZA - Foros</title>
<link rel="stylesheet" href="css/estilos.css" />
<link rel="stylesheet" href="css/print.css" media="print" />
<!--[if lt IE 9]>
<script src="script/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="script/menu.js"></script>
<script src="script/funciones.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="ckeditor/adapters/jquery.js"></script>
</head>
<body>
<div id="contenedor">
	<header id="cabecera">
		<h1><a href="index.php" title="AULA CERVEZA">AULA CERVEZA CREA TU PROPIA CERVEZA por Bob Maltman</a></h1>
		<div id="validacion">
			<div id="idiomas">
			<?php include("inc/idiomas.php"); ?>
			</div>
			<div id="registro">
			<?php include("inc/registro.php"); ?>
			</div>
		</div>
	</header>
	<nav id="navegacion">
		<div id="menu">
			<?php include("inc/menu.inc.php"); ?>
		</div>
	</nav>
	<section id="contenido">
		<nav id="imprimecomparte">
			<ul>
				<li><a href="#" id="imprimir">Imprimir</a></li>
				<li><a href="#" id="compartir">Compartir</a></li>
			</ul>
            <?php include("inc/inc.compartir.php"); ?>
		</nav>
		<section id="foros">
			<section id="foro">
			<?php
			$id_usuario_comentario = $_POST["identificacion_usuario"];
			$foro_comentario = trim($_POST["comentariosforo"]);
			$envio=1;

			if (esVacio($foro_comentario)) {
				$envio=0;
				$msg="Debes escribir un texto para que sea considerado un comentario";
			}

			if ($envio) {
				$query="INSERT INTO foros_comentarios (id_foro, id_usuario, comentario, publicado, fecha_publicacion) VALUES (".$foro.",".$id_usuario_comentario.",'".utf8_decode($foro_comentario)."','si', now())";
				$result=mysql_query($query);
				mysql_close($link);
					if ($result){
					?>
					<script type="text/javascript">
						document.location.href="foros-descripcion.php?foro=<?php echo $foro; ?>&pagina=<?php echo $pagina; ?>&pageforo=<?php echo $pageforo; ?>&mensaje=2";
					</script>
					<?php
					} else {
					?>
					<script type="text/javascript">
						document.location.href="foros-descripcion.php?foro=<?php echo $foro; ?>&pagina=<?php echo $pagina; ?>&pageforo=<?php echo $pageforo; ?>&mensaje=3";
					</script>
					<?php
					}
			} else {
			?>
			<form method="post" action="foros-descripcion.php?foro=<?php echo $foro; ?>&pagina=<?php echo $pagina; ?>&pageforo=<?php echo $pageforo; ?>" id="form" name="form" accept-charset="utf-8">
			<input type="hidden" name="foro_comentario" value="<?php echo $foro_comentario; ?>" />
			<input type="hidden" name="msg" value="<?php echo $msg; ?>" />
			</form>
			<script type="text/javascript">
				document.form.submit();
			</script>
			<?php
			}
			?>
			</section>
		</section>
	</section>
</div>

</body>
</html>

